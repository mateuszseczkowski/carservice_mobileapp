package seczkowski.mateusz.carservice.interfaces;

import seczkowski.mateusz.carservice.model.User;

public interface IPutPersonalUserDataAPICallback {
    void updateUserDataServiceSuccess(User user);
    void updateUserDataServiceFail(Exception exception);
}
