package seczkowski.mateusz.carservice.interfaces;

import seczkowski.mateusz.carservice.model.Repairs;

public interface IGetRepairsAPICallback {
    void serviceSuccess(Repairs repairs);
    void serviceFail(Exception exception);
}
