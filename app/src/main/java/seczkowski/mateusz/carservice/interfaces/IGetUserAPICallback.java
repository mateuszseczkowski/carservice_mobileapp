package seczkowski.mateusz.carservice.interfaces;

import javax.security.auth.login.LoginException;

import seczkowski.mateusz.carservice.model.User;

public interface IGetUserAPICallback {
    void getUserDataServiceSuccess(User user);
    void getUserDataServiceFail(Exception exception);
}
