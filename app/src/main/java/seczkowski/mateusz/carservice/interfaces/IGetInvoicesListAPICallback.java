package seczkowski.mateusz.carservice.interfaces;

import seczkowski.mateusz.carservice.model.InvoicesList;

public interface IGetInvoicesListAPICallback {
    void getInvoicesServiceSuccess(InvoicesList invoicesList);
    void serviceFail(Exception exception);
}
