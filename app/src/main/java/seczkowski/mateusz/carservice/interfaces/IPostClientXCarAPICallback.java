package seczkowski.mateusz.carservice.interfaces;

public interface IPostClientXCarAPICallback {
    void serviceSuccess(int clientXCarID);
    void serviceFail(Exception exception);
}
