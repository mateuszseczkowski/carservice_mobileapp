package seczkowski.mateusz.carservice.interfaces;

import seczkowski.mateusz.carservice.model.RepairOfflineStatus;

public interface IGetRepairOfflineStatusAPICallback {
    void checkRepairStatusServiceSuccess(RepairOfflineStatus repair);
    void checkRepairStatusServiceFail(Exception exception);
}
