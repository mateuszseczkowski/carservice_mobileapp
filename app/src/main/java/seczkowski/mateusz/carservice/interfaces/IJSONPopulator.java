package seczkowski.mateusz.carservice.interfaces;

import org.json.JSONArray;
import org.json.JSONObject;

public interface IJSONPopulator {
    void populate (JSONObject jObject);
    void populate (JSONArray jArray);
}
