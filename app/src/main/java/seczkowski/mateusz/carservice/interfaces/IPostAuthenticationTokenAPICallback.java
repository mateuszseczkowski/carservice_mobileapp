package seczkowski.mateusz.carservice.interfaces;

public interface IPostAuthenticationTokenAPICallback {
    void authenticationServiceSuccess(String accessToken);
    void authenticationServiceFail(Exception exception);
}
