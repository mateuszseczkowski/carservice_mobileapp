package seczkowski.mateusz.carservice.interfaces;

import seczkowski.mateusz.carservice.model.Mechanic;

public interface IGetMechanicsAPICallback {
    void getMechanicsServiceSuccess(Mechanic mechanic);
    void getMechanicsServiceFail(Exception exception);
}
