package seczkowski.mateusz.carservice.interfaces;

import seczkowski.mateusz.carservice.model.IncomingRepairsList;

public interface IGetIncomingRepairsListAPICallback {
    void getIncomingRepairsServiceSuccess(IncomingRepairsList incomingRepairsList);
    void serviceFail(Exception exception);
}
