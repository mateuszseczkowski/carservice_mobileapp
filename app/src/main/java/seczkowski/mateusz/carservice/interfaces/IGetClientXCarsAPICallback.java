package seczkowski.mateusz.carservice.interfaces;

import java.util.List;

import seczkowski.mateusz.carservice.model.ClientXCar;
import seczkowski.mateusz.carservice.model.Repair;

public interface IGetClientXCarsAPICallback {
    void getClientsCarsListServiceSuccess(ClientXCar clientXCar);
    void getClientsCarsListServiceFail(Exception exception);
}
