package seczkowski.mateusz.carservice.interfaces;

import seczkowski.mateusz.carservice.model.RepairRequest;

public interface IPostRepairRequestAPICallback {
    void addRepairRequestServiceSuccess(RepairRequest repairRequest);
    void addRepairRequestServiceFail(Exception exception);
}
