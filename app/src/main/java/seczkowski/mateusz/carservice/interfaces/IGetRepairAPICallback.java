package seczkowski.mateusz.carservice.interfaces;

import seczkowski.mateusz.carservice.model.Repair;

public interface IGetRepairAPICallback {
    void serviceSuccess(Repair repair);
    void serviceFail(Exception exception);
}
