package seczkowski.mateusz.carservice.interfaces;

import seczkowski.mateusz.carservice.model.ClientXCar;

public interface IPutClientXCarFlagsAPICallback {
    void updateClientXCarFlagsServiceSuccess(ClientXCar clientXCar);
    void updateClientXCarFlagsServiceFail(Exception exception);
}
