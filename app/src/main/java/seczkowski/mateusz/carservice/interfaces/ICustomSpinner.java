package seczkowski.mateusz.carservice.interfaces;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public interface ICustomSpinner {
    View getCustomSpinner(int position, View convertView, @NonNull ViewGroup parent);
}
