package seczkowski.mateusz.carservice.helpers;

import java.util.Comparator;
import seczkowski.mateusz.carservice.model.Repair;

import static seczkowski.mateusz.carservice.helpers.Enums.OrderType.*;

public class RepairesComparator implements Comparator<Repair> {

private Enum orderType;

public RepairesComparator(Enum type) {
    this.orderType = type;
}

public int compare(Repair lhs, Repair rhs) {
    int res = 0;
    if (orderType == START_DATE_ASC) {
        res = (lhs.getStartDate()).compareTo(rhs.getStartDate());
    }
    else if (orderType == START_DATE_DESC) {
        res = (rhs.getStartDate()).compareTo(lhs.getStartDate());
    }
    else if (orderType == ID_ASC) {
        res = ((Integer)lhs.getRepairID()).compareTo(rhs.getRepairID());
    }
    else if (orderType == ID_DESC) {
        res = ((Integer)rhs.getRepairID()).compareTo(lhs.getRepairID());
    }
    return res;
    }
}