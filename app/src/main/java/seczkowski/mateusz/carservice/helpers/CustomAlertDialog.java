package seczkowski.mateusz.carservice.helpers;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import seczkowski.mateusz.carservice.R;

public class CustomAlertDialog {
    public void showMessageYesCancel(Activity activity, String message, DialogInterface.OnClickListener submitListener) {
        new AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton(R.string.yes, submitListener)
                .setNegativeButton(R.string.cancel, null)
                .create()
                .show();
    }

    public void showMessageYesNo(Activity activity, String message, DialogInterface.OnClickListener submitListener) {
        new AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton(R.string.yes, submitListener)
                .setNegativeButton(R.string.no, null)
                .create()
                .show();
    }

    public void showMessageOk(Activity activity, String message) {
        new AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton(R.string.ok, null)
                .show();
    }
}
