package seczkowski.mateusz.carservice.helpers;

import android.content.Context;

import seczkowski.mateusz.carservice.R;

public class ToStringHelper {
    public static String ObjectDataToString(Context context, Object object) {
        if (object instanceof Boolean) {
            if ((Boolean)object)
                return context.getString(R.string.bool_true);
            else return context.getString(R.string.bool_false);
        }
        if (object == null || object.toString().equals("null")) {
            return context.getString(R.string.null_text);
        }
        else {
            return object.toString();
        }
    }
}
