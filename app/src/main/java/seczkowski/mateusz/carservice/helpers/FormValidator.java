package seczkowski.mateusz.carservice.helpers;

import android.app.Activity;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import seczkowski.mateusz.carservice.R;

public class FormValidator {
    public static boolean validateRequiredFields(List<EditText> requiredFieldsList, Activity activity) {
        int fieldsValidationErrorsCount = 0;

        for (EditText requiredField : requiredFieldsList) {
            if(requiredField.getText().toString().trim().equals("")){
                requiredField.setError(requiredField.getHint().toString() + " is required");
                fieldsValidationErrorsCount++;
            }
        }

        if(fieldsValidationErrorsCount != 0){
            Toast.makeText(activity, R.string.fix_validation_errors, Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}