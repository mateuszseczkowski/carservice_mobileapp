package seczkowski.mateusz.carservice.helpers.Data;

import android.os.AsyncTask;
import android.util.Log;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import seczkowski.mateusz.carservice.model.ClientXCar;
import seczkowski.mateusz.carservice.model.Repair;
import seczkowski.mateusz.carservice.model.Repairs;

public class SQLiteDatabaseWrapper {
    String LOG_TAG = "SQLiteDatabaseWrapper";
    DBHelper db = null;

    public SQLiteDatabaseWrapper(DBHelper db) {
        this.db = db;
    }

    public void putRepairsListLiteAsync(final List<Repair> repairsList) { // "...Lite suffix in the method name indicates that the database object is without child objects. Example: Parent in this case is Repair and Child is ClientXCar
        new AsyncTask<Integer, Void, String>() { //do it in background as there is no need to hold UI thread
            @Override
            protected String doInBackground(Integer... params) {
                try {
                    Dao<Repair, Integer> repairsDao = db.getRepairsDao();
                    List<Repair> repairs = repairsDao.queryForAll();

                    for (Repair repair : repairs) {
                        repairsDao.delete(repair); // We do not need to store all the historical objects. Thus, we delete the objects from the DB each time when the PUT method witch the new ones is called.
                    }

                    for (Repair r : repairsList) {
                        repairsDao.create(r);
                        try {
                            Thread.sleep(5);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } finally {
                            Log.i(LOG_TAG, "Repair " + r.getRepairID() + " saved in the database");
                        }
                    }
                } catch (SQLException e) {
                    Log.e(LOG_TAG, " Filling the database failed: " + e.getMessage());
                }
                return null;
            }
        }.execute();
    }

    public void putClientXCarLite(ClientXCar cxc) {
    }

    public Repair getRepairLite(int repairID) {
        try {
            Dao<Repair, Integer> repairsDao = db.getRepairsDao();

            return repairsDao.queryForEq("RepairID", repairID).get(0);
        } catch (SQLException e) {
            Log.e(LOG_TAG, " Obtaining the database objects failed: " + e.getMessage());
        }
        return new Repair();
    }

    public List<Repair> getRepairsListLite() {
        List<Repair> repairsList = new ArrayList<>();

        try {
            Dao<Repair, Integer> repairsDao = db.getRepairsDao();
            repairsList = repairsDao.queryForAll();
        } catch (SQLException e) {
            Log.e(LOG_TAG, " Obtaining the database objects failed: " + e.getMessage());
            return repairsList;
        }
        Log.i(LOG_TAG, "Repairs list obtained from the database");

        return repairsList;
    }
}
