package seczkowski.mateusz.carservice.helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateParserFormatterHelper {
    public static Date ParseDateFromString(String input) throws ParseException {
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        return parser.parse(input);
    }
    public static String ToString(Date input) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        if (input == null)
            return "-";
        return formatter.format(input);
    }
}
