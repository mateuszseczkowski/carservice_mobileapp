package seczkowski.mateusz.carservice.helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkConnectionHelper {

    private boolean isNetworkConnection = false;

    public boolean isOnline() {
        return isNetworkConnection;
    }

    public NetworkConnectionHelper(Context context) {
        isNetworkConnection = checkIfTheDeviceIsOnline(context);
    }

    private boolean checkIfTheDeviceIsOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}