package seczkowski.mateusz.carservice.adapters;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import seczkowski.mateusz.carservice.R;
import seczkowski.mateusz.carservice.helpers.DateParserFormatterHelper;
import seczkowski.mateusz.carservice.helpers.CustomAlertDialog;
import seczkowski.mateusz.carservice.helpers.ToStringHelper;
import seczkowski.mateusz.carservice.model.RepairOfflineStatus;

public class RepairStatusDialogAdapter implements ActivityCompat.OnRequestPermissionsResultCallback {
    private Context context;
    private RepairStatusDialogHolder holder = null;
    private Activity activity;
    private AlertDialog alertDialog;
    private RepairOfflineStatus object;
    private int statusColor;

    public RepairStatusDialogAdapter(Context context) {
        this.context = context;
    }

    public void initView(View v, Activity activity, AlertDialog alertDialog) {
        holder = new RepairStatusDialogHolder();
        this.activity = activity;
        this.alertDialog = alertDialog;

        holder.statusHash = (TextView) v.findViewById(R.id.repair_status_hash);
        holder.statusMechanic = (TextView) v.findViewById(R.id.repair_status_mechanic);
        holder.statusCar = (TextView) v.findViewById(R.id.repair_status_car);
        holder.statusPlannedEndDate = (TextView) v.findViewById(R.id.repair_status_plannedenddate);
        holder.statusIsFinished = (TextView) v.findViewById(R.id.repair_status_isfinished);
        holder.statusDescription = (TextView) v.findViewById(R.id.repair_status_description);
        holder.buttonCall = (Button) v.findViewById(R.id.call_to_mechanic);
    }

    public void setData(RepairOfflineStatus object) {
        this.object = object;

        holder.statusHash.setText(String.format("%s: %s", context.getString(R.string.repair_hash),
                ToStringHelper.ObjectDataToString(context, object.getRepairHash())));
        holder.statusMechanic.setText(String.format("%s: \n%s", context.getString(R.string.repair_status_mechanic),
                ToStringHelper.ObjectDataToString(context, object.getMechanic().getUser().getNameAndSurname() + "\n" + object.getMechanic().getUser().getPhone())));

        checkPermisionsAndSetListenerForCallToMechanic();

        holder.statusCar.setText(String.format("%s: %s", context.getString(R.string.car_title),
                ToStringHelper.ObjectDataToString(context, object.getClientXCar().getCar().getCarBasicData())));
        holder.statusPlannedEndDate.setText(String.format("%s: %s", context.getString(R.string.repair_PlannedEndDate),
                DateParserFormatterHelper.ToString(object.getPlannedEndDate())));
        holder.statusIsFinished.setText(String.format("%s: %s", context.getString(R.string.repair_isfinished_title),
                ToStringHelper.ObjectDataToString(context, object.isFinished())));
        holder.statusDescription.setText(String.format("%s: %s", context.getString(R.string.repair_status_description),
                ToStringHelper.ObjectDataToString(context, object.getDescription())));

        if (object.isFinished()) {
            statusColor = ContextCompat.getColor(context, R.color.materialLightGreen900AlphaAA);
        }
        else {
            statusColor = ContextCompat.getColor(context, R.color.materialRed500Alpha74);
        }
        holder.statusHash.setBackgroundColor(statusColor);
        holder.statusIsFinished.setBackgroundColor(statusColor);
    }

    private class RepairStatusDialogHolder {
        TextView  statusHash, statusPlannedEndDate, statusIsFinished, statusDescription, statusMechanic, statusCar;
        Button buttonCall;
    }

    private void checkPermisionsAndSetListenerForCallToMechanic() {
        holder.buttonCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    askPermissions();
                }
                else {
                    callMechanic();
                }
                alertDialog.dismiss();
            }
        });
    }

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    private void askPermissions() {
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.CALL_PHONE},
                REQUEST_CODE_ASK_PERMISSIONS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callMechanic();
                }
            }
        }
    }

    private void callMechanic() {
        final Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse(String.format("tel:%s", object.getMechanic().getUser().getPhone())));

        new CustomAlertDialog().showMessageYesCancel(activity,
                context.getString(R.string.constraints_call_decision) + object.getMechanic().getUser().getNameAndSurname() + "?",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        context.startActivity(callIntent);
                    }
                });
    }
}
