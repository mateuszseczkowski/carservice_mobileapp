package seczkowski.mateusz.carservice.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import seczkowski.mateusz.carservice.R;
import seczkowski.mateusz.carservice.model.DrawerNavItem;

public class DrawerNavListAdapter extends ArrayAdapter<DrawerNavItem> {

    private Context context;
    private int resLayout;
    private List<DrawerNavItem> drawerNavItems;

    public DrawerNavListAdapter(Context context, int resLayout, List<DrawerNavItem> drawerNavItems) {
        super(context, resLayout, drawerNavItems);
        this.context = context;
        this.resLayout = resLayout;
        this.drawerNavItems = drawerNavItems;
    }

    @NonNull
    @SuppressLint("ViewHolder") @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        View v = View.inflate(context, resLayout, null);

        TextView tvTitle = (TextView) v.findViewById(R.id.title);
        TextView tvSubTitle = (TextView) v.findViewById(R.id.subtitle);
        ImageView navIcon = (ImageView) v.findViewById(R.id.nav_icon);

        DrawerNavItem drawerNavItem = drawerNavItems.get(position);

        tvTitle.setText(drawerNavItem.getTitle());
        tvSubTitle.setText(drawerNavItem.getSubtTitle());
        navIcon.setImageResource(drawerNavItem.getResIcon());

        return v;
    }
}


