package seczkowski.mateusz.carservice.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import seczkowski.mateusz.carservice.R;
import seczkowski.mateusz.carservice.helpers.DateParserFormatterHelper;
import seczkowski.mateusz.carservice.helpers.ToStringHelper;
import seczkowski.mateusz.carservice.model.IncomingRepair;
import seczkowski.mateusz.carservice.model.Invoice;

public class IncomingRepairsListItemAdapter extends ArrayAdapter<IncomingRepair> {
    private Context context;
    private int layoutResourceId;
    private IncomingRepair data[] = null;
    private Activity activity;

    public IncomingRepairsListItemAdapter(Context context, int layoutResourceId, IncomingRepair[] data, Activity activity) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
        this.activity = activity;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        IncomingRepairHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new IncomingRepairHolder();

            holder.id = (TextView) row.findViewById(R.id.id);
            holder.incoming_repair_datetime = (TextView) row.findViewById(R.id.incoming_repair_DateTime);
            holder.incoming_repair_message = (TextView) row.findViewById(R.id.incoming_repair_message);

            row.setTag(holder);
        } else {
            holder = (IncomingRepairHolder) row.getTag();
        }

        IncomingRepair object = data[position];

        holder.id.setText(String.format("%s: %s", "", ToStringHelper.ObjectDataToString(context, object.getId())));
        holder.incoming_repair_datetime.setText(String.format("%s: %s", "Date time",
                DateParserFormatterHelper.ToString(object.getIncomingRepairDateTime())));
        holder.incoming_repair_message.setText(ToStringHelper.ObjectDataToString(context, object.getMessageFromClient()));

        return row;
    }

    private static class IncomingRepairHolder {
        TextView id;
        TextView incoming_repair_datetime;
        TextView incoming_repair_message;
    }
}
