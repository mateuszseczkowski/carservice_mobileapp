package seczkowski.mateusz.carservice.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.Arrays;
import java.util.zip.Inflater;

import seczkowski.mateusz.carservice.R;
import seczkowski.mateusz.carservice.interfaces.ICustomSpinner;
import seczkowski.mateusz.carservice.model.ClientXCar;

public class SpinnerClientXCarsAdapter extends ArrayAdapter<ClientXCar> implements ICustomSpinner {
    private Context context;
    private ClientXCar[] values;
    private LayoutInflater inflater;

    public SpinnerClientXCarsAdapter(Context context, int textViewResourceId, ClientXCar[] values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount(){
       return values.length;
    }

    public ClientXCar getItem(int position){
       return values[position];
    }

    public long getItemId(int position){
       return values[position].getClientXCarId();
    }

    public int getDefaultCarPosition() {
        for (ClientXCar car: values) {
            if (car.isDefault()) {
                return Arrays.asList(values).indexOf(car);
            }
        }
        return -1;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        return getCustomSpinner(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        return getCustomSpinner(position, convertView, parent);
    }

    public View getCustomSpinner(int position, View convertView, @NonNull ViewGroup parent) {
        View singleSpinner = inflater.inflate(R.layout.spinner_with_label, parent, false);

        TextView value = (TextView) singleSpinner.findViewById(R.id.spinner_value);
        TextView label = (TextView) singleSpinner.findViewById(R.id.spinner_label);

        value.setText(values[position].getClientXCarBasicData());
        label.setText(context.getString(R.string.select_car));

        return singleSpinner;
    }
}