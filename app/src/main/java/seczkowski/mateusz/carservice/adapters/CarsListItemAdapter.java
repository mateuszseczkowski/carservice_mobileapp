package seczkowski.mateusz.carservice.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import seczkowski.mateusz.carservice.R;
import seczkowski.mateusz.carservice.helpers.ToStringHelper;
import seczkowski.mateusz.carservice.model.ClientXCar;

public class CarsListItemAdapter extends ArrayAdapter<ClientXCar> {
    private Context context;
    private int layoutResourceId;
    private ClientXCar data[] = null;
    private Activity activity;

    public CarsListItemAdapter(Context context, int layoutResourceId, ClientXCar[] data, Activity activity) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
        this.activity = activity;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        RepairHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new RepairHolder();

            holder.plateNo = (TextView) row.findViewById(R.id.car_plateNo);
            holder.carId = (TextView) row.findViewById(R.id.car_id);
            holder.carModel = (TextView) row.findViewById(R.id.car_model);
            holder.carMake = (TextView) row.findViewById(R.id.car_make);
            holder.carYear = (TextView) row.findViewById(R.id.car_year);
            holder.isActive = (TextView) row.findViewById(R.id.car_is_active);

            row.setTag(holder);

        } else {
            holder = (RepairHolder) row.getTag();
        }

        ClientXCar object = data[position];

        holder.plateNo.setText(String.format("%s", ToStringHelper.ObjectDataToString(context, object.getPlateNo())));
        holder.carModel.setText(String.format("%s: %s", context.getString(R.string.Car_Model),
                ToStringHelper.ObjectDataToString(context, object.getCar().getModel())));
        holder.carMake.setText(String.format("%s: %s", context.getString(R.string.Car_Make),
                ToStringHelper.ObjectDataToString(context,object.getCar().getMake())));
        holder.carYear.setText(String.format("%s: %s", context.getString(R.string.Car_Year),
                ToStringHelper.ObjectDataToString(context, object.getCar().getYear())));
        holder.isActive.setText(String.format("%s: %s", context.getString(R.string.repair_car_is_active),
                ToStringHelper.ObjectDataToString(context, object.isActive())));

        if (object.isActive()) {
            row.setBackgroundColor(ContextCompat.getColor(context, R.color.materialLightGreen900AlphaAA)); // material design colors
        } else {
            row.setBackgroundColor(ContextCompat.getColor(context, R.color.materialGrey800Alpha74));
        }

        return row;
    }

    private static class RepairHolder {
        TextView plateNo, carId, carModel, carMake, carYear, isActive;
    }
}
