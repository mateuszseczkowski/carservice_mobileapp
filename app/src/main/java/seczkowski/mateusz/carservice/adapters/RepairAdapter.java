package seczkowski.mateusz.carservice.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import seczkowski.mateusz.carservice.R;
import seczkowski.mateusz.carservice.helpers.ToStringHelper;
import seczkowski.mateusz.carservice.helpers.DateParserFormatterHelper;
import seczkowski.mateusz.carservice.model.Repair;

public class RepairAdapter {
    private Context context;
    private RepairActivityHolder holder = null;
    private int statusColor;

    public RepairAdapter(Context context) {
        this.context = context;
    }

    public void initView(View v) {
            holder = new RepairActivityHolder();

            holder.repairHash = (TextView) v.findViewById(R.id.repair_act_hash);
            holder.repairStartDate = (TextView) v.findViewById(R.id.repair_act_start_date);
            holder.repairIsFinished = (TextView) v.findViewById(R.id.repair_act_isfinished);
            holder.repairIsFinishedContainer = (RelativeLayout) v.findViewById(R.id.rel_3_is_finished_background);
            holder.repairDescription = (TextView) v.findViewById(R.id.repair_act_description);
            holder.repairRequestDate = (TextView) v.findViewById(R.id.repair_act_request_date);
            holder.repairPlanEndDate = (TextView) v.findViewById(R.id.repair_act_planed_end_date);
            holder.repairCarPlateNo = (TextView) v.findViewById(R.id.repair_act_car_plate);
            holder.repairCarModel = (TextView) v.findViewById(R.id.repair_act_car_model);
            holder.repairCarMake = (TextView) v.findViewById(R.id.repair_act_car_make);
            holder.repairCarYear = (TextView) v.findViewById(R.id.repair_act_car_year);
            holder.repairCarCapacity = (TextView) v.findViewById(R.id.repair_act_car_capacity);
            holder.repairCarBhp = (TextView) v.findViewById(R.id.repair_act_car_bhp);
            holder.repairCarVersion = (TextView) v.findViewById(R.id.repair_act_car_version);
            holder.repairCarVinNo = (TextView) v.findViewById(R.id.repair_act_car_vinno);
    }

    public void setData(Repair object) {
        holder.repairHash.setText(
                ToStringHelper.ObjectDataToString(context, object.getRepairHash()));
        holder.repairStartDate.setText(String.format("%s: %s", context.getString(R.string.repair_start_date),
                DateParserFormatterHelper.ToString(object.getStartDate())));
        holder.repairIsFinished.setText(String.format("%s: %s", context.getString(R.string.repair_isfinished_title),
                ToStringHelper.ObjectDataToString(context, object.isFinished())));
        holder.repairDescription.setText(
                ToStringHelper.ObjectDataToString(context, object.getDescription()));
        holder.repairRequestDate.setText(String.format("%s: %s", context.getString(R.string.repair_RequestDate),
                DateParserFormatterHelper.ToString(object.getRequestDate())));
        holder.repairPlanEndDate.setText(String.format("%s: %s", context.getString(R.string.repair_PlannedEndDate),
                DateParserFormatterHelper.ToString(object.getPlannedEndDate())));
        holder.repairCarPlateNo.setText(String.format("%s: %s", context.getString(R.string.CarxClient_PlateNo),
                ToStringHelper.ObjectDataToString(context, object.getClientXCar().getPlateNo())));
        holder.repairCarModel.setText(String.format("%s: %s", context.getString(R.string.Car_Model),
                ToStringHelper.ObjectDataToString(context, object.getClientXCar().getCar().getModel())));
        holder.repairCarMake.setText(String.format("%s: %s", context.getString(R.string.Car_Make),
                ToStringHelper.ObjectDataToString(context, object.getClientXCar().getCar().getMake())));
        holder.repairCarYear.setText(String.format("%s: %s", context.getString(R.string.Car_Year),
                ToStringHelper.ObjectDataToString(context, object.getClientXCar().getCar().getYear())));
        holder.repairCarCapacity.setText(String.format("%s: %s", context.getString(R.string.Car_Capacity),
                ToStringHelper.ObjectDataToString(context, object.getClientXCar().getCar().getCapacity().toString())));
        holder.repairCarBhp.setText(String.format("%s: %s", context.getString(R.string.Car_Bhp),
                ToStringHelper.ObjectDataToString(context, object.getClientXCar().getCar().getBhp().toString())));
        holder.repairCarVersion.setText(String.format("%s: %s", context.getString(R.string.Car_Version),
                ToStringHelper.ObjectDataToString(context, object.getClientXCar().getCar().getVersion())));
        holder.repairCarVinNo.setText(String.format("%s: %s", context.getString(R.string.Car_VinNo),
                ToStringHelper.ObjectDataToString(context, object.getClientXCar().getCar().getVinNo())));

        if (object.isFinished()) {
            statusColor = ContextCompat.getColor(context, R.color.materialLightGreen900AlphaAA);
        }
        else {
            statusColor = ContextCompat.getColor(context, R.color.materialRed500Alpha74);
        }
        holder.repairHash.setBackgroundColor(statusColor);
        holder.repairIsFinishedContainer.setBackgroundColor(statusColor);
    }

    private class RepairActivityHolder {
        RelativeLayout repairIsFinishedContainer;
        TextView repairHash,
                repairStartDate,
                repairIsFinished,
                repairDescription,
                repairRequestDate,
                repairPlanEndDate,
                repairCarPlateNo,
                repairCarModel,
                repairCarMake,
                repairCarYear,
                repairCarCapacity,
                repairCarBhp,
                repairCarVersion,
                repairCarVinNo;
    }
}
