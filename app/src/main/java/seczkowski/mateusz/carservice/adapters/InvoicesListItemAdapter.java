package seczkowski.mateusz.carservice.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import seczkowski.mateusz.carservice.R;
import seczkowski.mateusz.carservice.helpers.DateParserFormatterHelper;
import seczkowski.mateusz.carservice.helpers.ToStringHelper;
import seczkowski.mateusz.carservice.model.Invoice;
import seczkowski.mateusz.carservice.model.Repair;

public class InvoicesListItemAdapter extends ArrayAdapter<Invoice> {
    private Context context;
    private int layoutResourceId;
    private Invoice data[] = null;
    private Activity activity;

    public InvoicesListItemAdapter(Context context, int layoutResourceId, Invoice[] data, Activity activity) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
        this.activity = activity;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        InvoiceHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new InvoiceHolder();

            holder.id = (TextView) row.findViewById(R.id.id);
            holder.invoice_repair_id = (TextView) row.findViewById(R.id.invoice_repair_id);
            holder.invoice_issueDate = (TextView) row.findViewById(R.id.invoice_issueDate);
            holder.invoice_grossAmount = (TextView) row.findViewById(R.id.invoice_grossAmount);
            holder.invoice_dueDate = (TextView) row.findViewById(R.id.invoice_dueDate);

            row.setTag(holder);
        } else {
            holder = (InvoiceHolder) row.getTag();
        }

        Invoice object = data[position];

        holder.id.setText(String.format("%s: %s", context.getString(R.string.invoice_id),
                ToStringHelper.ObjectDataToString(context, object.getInvoiceID())));
        holder.invoice_repair_id.setText(String.format("%s: %s", context.getString(R.string.repair_id),
                ToStringHelper.ObjectDataToString(context, object.getRepairID())));
        holder.invoice_issueDate.setText(String.format("%s: %s", context.getString(R.string.issue_date),
                DateParserFormatterHelper.ToString(object.getIssueDate())));
        holder.invoice_grossAmount.setText(String.format("%s: %s", context.getString(R.string.to_pay),
                ToStringHelper.ObjectDataToString(context, object.getGrossAmount())));
        holder.invoice_dueDate.setText(String.format("%s: %s", context.getString(R.string.due_date),
                DateParserFormatterHelper.ToString(object.getDueDate())));

        return row;
    }

    private static class InvoiceHolder {
        TextView id;
        TextView invoice_repair_id;
        TextView invoice_issueDate;
        TextView invoice_grossAmount;
        TextView invoice_dueDate;
    }
}
