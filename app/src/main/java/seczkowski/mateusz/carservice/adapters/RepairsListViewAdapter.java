package seczkowski.mateusz.carservice.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import seczkowski.mateusz.carservice.R;
import seczkowski.mateusz.carservice.helpers.RepairesComparator;
import seczkowski.mateusz.carservice.helpers.ToStringHelper;
import seczkowski.mateusz.carservice.helpers.DateParserFormatterHelper;
import seczkowski.mateusz.carservice.model.Repair;

import static seczkowski.mateusz.carservice.helpers.Enums.OrderType.*;

public class RepairsListViewAdapter extends ArrayAdapter<Repair> implements Filterable {
    private String LOG_TAG = "RepairsListItemAdapter";

    private Context context;
    private int layoutResourceId;
    private List<Repair> data;
    private List<Repair> filteredData;
    private Enum memorizedSortType = ID_ASC;
    private Activity activity;

    public RepairsListViewAdapter(Context context, int layoutResourceId, List<Repair> data, Activity activity) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
        this.filteredData = data;
        this.activity = activity;
    }

    public int getCount() {
        if (filteredData != null) {
            return filteredData.size();
        }
        return -1;
    }

    public Repair getItem(int position) {
        if (filteredData != null) {
            return filteredData.get(position);
        }
        return null;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        RepairHolder holder;

        if (row == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new RepairHolder();

            holder.description = (TextView) row.findViewById(R.id.description);
            holder.id = (TextView) row.findViewById(R.id.id);
            holder.repair_hash = (TextView) row.findViewById(R.id.repair_hash);
            holder.start_date = (TextView) row.findViewById(R.id.start_date);
            holder.end_date = (TextView) row.findViewById(R.id.end_date);
            holder.is_finished = (TextView) row.findViewById(R.id.is_finished);

            row.setTag(holder);
        } else {
            holder = (RepairHolder) row.getTag();
        }

        Repair object = filteredData.get(position);

        holder.description.setText(
                ToStringHelper.ObjectDataToString(context, object.getDescription()));
        holder.id.setText(String.format("%s: %s", context.getString(R.string.repair_id),
                ToStringHelper.ObjectDataToString(context, object.getRepairID())));
        holder.repair_hash.setText(
                ToStringHelper.ObjectDataToString(context, object.getRepairHash()));
        holder.start_date.setText(String.format("%s: %s", context.getString(R.string.repair_start_date),
                DateParserFormatterHelper.ToString(object.getStartDate())));
        holder.end_date.setText(String.format("%s: %s", context.getString(R.string.repair_end_date),
                DateParserFormatterHelper.ToString(object.getPlannedEndDate())));
        holder.is_finished.setText(String.format("%s: %s", context.getString(R.string.repair_isfinished_title),
                ToStringHelper.ObjectDataToString(context, object.isFinished())));

        return row;
    }

    public void sort(Enum sortType)
    {
        memorizedSortType = sortType;
        try {
            Collections.sort(filteredData, new RepairesComparator(sortType));
        } catch (NullPointerException e) {
            Log.e(LOG_TAG, e.getMessage());
            e.printStackTrace();
        } finally {
            notifyDataSetChanged();
        }
    }

    @NonNull
    public Filter getFilter() {
        return new RepairsFilter();
    }

    private class RepairsFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<Repair> originalDataList = data;

            int count = originalDataList.size();
            final ArrayList<Repair> newListOfRepairs = new ArrayList<>(count);

            for (Repair aRepair : originalDataList) {
                if (aRepair.toFilterString().toLowerCase().contains(filterString)) {
                    newListOfRepairs.add(aRepair);
                }
            }

            results.values = newListOfRepairs;
            results.count = newListOfRepairs.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<Repair>) results.values;
            sort(memorizedSortType);
            notifyDataSetChanged();
        }
    }

    private static class RepairHolder {
        TextView id, repair_hash, start_date, end_date, is_finished, description;
    }
}
