package seczkowski.mateusz.carservice.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import seczkowski.mateusz.carservice.R;
import seczkowski.mateusz.carservice.interfaces.ICustomSpinner;
import seczkowski.mateusz.carservice.model.ClientXCar;
import seczkowski.mateusz.carservice.model.Mechanic;

public class SpinnerMechanicsAdapter extends ArrayAdapter<Mechanic> implements ICustomSpinner {
    private Context context;
    private Mechanic[] values;
    private LayoutInflater inflater;

    public SpinnerMechanicsAdapter(Context context, int textViewResourceId, Mechanic[] values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount(){
       return values.length;
    }

    public Mechanic getItem(int position){
       return values[position];
    }

    public long getItemId(int position){
       return values[position].getMechanicId();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        return getCustomSpinner(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        return getCustomSpinner(position, convertView, parent);
    }

    public View getCustomSpinner(int position, View convertView, @NonNull ViewGroup parent) {
        View singleSpinner = inflater.inflate(R.layout.spinner_with_label, parent, false);

        TextView value = (TextView) singleSpinner.findViewById(R.id.spinner_value);
        TextView label = (TextView) singleSpinner.findViewById(R.id.spinner_label);

        value.setText(values[position].getUser().getNameAndSurname());
        label.setText(context.getString(R.string.select_mechanic));

        return singleSpinner;
    }
}