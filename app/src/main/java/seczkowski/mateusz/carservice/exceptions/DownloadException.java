package seczkowski.mateusz.carservice.exceptions;

public class DownloadException extends Exception {
        public DownloadException(String detailMessage) {
            super(detailMessage);
        }
}