package seczkowski.mateusz.carservice.exceptions;

import android.app.Activity;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.Toast;

import seczkowski.mateusz.carservice.helpers.CustomAlertDialog;

public class ServiceFailedHelper {

    private String message;
    private Activity activity;

    public ServiceFailedHelper(Exception exception, Activity activity) {
        this.message = null;
        this.activity = activity;
        processException(exception);
    }

    public ServiceFailedHelper(Exception exception, String message, Activity activity) {
        this.message = message;
        this.activity = activity;
        processException(exception);
    }

    private void processException(Exception exception) {
        exception.printStackTrace();
        try {
            String exceptionMessage = exception.getMessage();
            if (message == null)
                message = exceptionMessage;
            Log.e("Service exception", exceptionMessage);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showDialogOk(){
        new CustomAlertDialog()
                .showMessageOk(activity, message);
    }

    public void showDialogYesCancel(DialogInterface.OnClickListener submitListener){
        if (submitListener != null) {
            new CustomAlertDialog()
                    .showMessageYesCancel(activity, message, submitListener);
        }
        else {
            Log.i("Dialog info", "Yes button listener is not specified. The 'OK' dialog will be displayed instead");
            this.showDialogOk();
        }
    }

    public void showToast(){
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }
}