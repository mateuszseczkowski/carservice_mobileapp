package seczkowski.mateusz.carservice.services;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;

import seczkowski.mateusz.carservice.exceptions.DownloadException;
import seczkowski.mateusz.carservice.interfaces.IPostRepairRequestAPICallback;
import seczkowski.mateusz.carservice.model.RepairRequest;

public class PostRepairRequestAPIService extends ApiService {

    IPostRepairRequestAPICallback callback;
    int userId;
    private Exception error;

    public PostRepairRequestAPIService(IPostRepairRequestAPICallback callback, Context context) {
        super(context);
        this.callback = callback;
    }

    public void sendRepairRequest(final RepairRequest repairRequest, int userId) {

        this.userId = userId;

        new AsyncTask<Integer, Void, String>() {

            @Override
            protected String doInBackground(Integer... integers) {
                try {
                    URL url = new URL(String.format(API_BaseURL() + "api/rest/incomingrepairs?userId=%s", integers[0]));

                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    return oAuthAuthorizeSendRequestAndGetResult(connection, "mechanicId=" + repairRequest.getMechanicId()
                            + "&clientXCarId=" + repairRequest.getClientXCarId()
                            + "&messageFromClient=" + repairRequest.getMessageFromClient(), "POST");

                } catch (Exception e) {
                    error = e;
                }

                return null;
            }

            @Override
            protected void onPostExecute(String s) {

                if (s == null && error != null) {
                    callback.addRepairRequestServiceFail(error);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(s);

                    if (object.has("message")) {
                        callback.addRepairRequestServiceFail(new DownloadException(object.optString("message")));
                        return;
                    }

                    RepairRequest repairRequest = new RepairRequest();
                    repairRequest.populate(object);

                    callback.addRepairRequestServiceSuccess(repairRequest);

                } catch (JSONException e) {
                    callback.addRepairRequestServiceFail(e);
                }
            }
        }.execute(this.userId);
    }
}
