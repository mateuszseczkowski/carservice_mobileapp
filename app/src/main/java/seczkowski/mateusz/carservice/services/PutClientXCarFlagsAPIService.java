package seczkowski.mateusz.carservice.services;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;

import seczkowski.mateusz.carservice.exceptions.DownloadException;
import seczkowski.mateusz.carservice.interfaces.IPutClientXCarFlagsAPICallback;
import seczkowski.mateusz.carservice.model.ClientXCar;

public class PutClientXCarFlagsAPIService extends ApiService {

    IPutClientXCarFlagsAPICallback callback;
    ClientXCar cxc;
    private Exception error;

    public PutClientXCarFlagsAPIService(IPutClientXCarFlagsAPICallback callback, Context context) {
        super(context);
        this.callback = callback;
    }

    public void updateClientXCarFlags(ClientXCar cxc) {
        this.cxc = cxc;

        new AsyncTask<ClientXCar, Void, String>() {

            @Override
            protected String doInBackground(ClientXCar... clientXCars) {
                try {
                    URL url = new URL(String.format(API_BaseURL() + "api/rest/clientxcars/%s?isActive=%s&isDefault=%s",
                            clientXCars[0].getClientXCarId(), clientXCars[0].isActive(), clientXCars[0].isDefault()));

                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    return oAuthAuthorizeSendRequestAndGetResult(connection, null, "PUT");
                } catch (Exception e) {
                    error = e;
                }

                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                if (s == null && error != null) {
                    callback.updateClientXCarFlagsServiceFail(error);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(s);

                    if (object.has("message")) {
                        callback.updateClientXCarFlagsServiceFail(new DownloadException(object.optString("message")));
                        return;
                    }

                    ClientXCar cxc = new ClientXCar();
                    cxc.populate(object);

                    callback.updateClientXCarFlagsServiceSuccess(cxc);

                } catch (JSONException e) {
                    callback.updateClientXCarFlagsServiceFail(e);
                }
            }
        }.execute(cxc);
    }
}
