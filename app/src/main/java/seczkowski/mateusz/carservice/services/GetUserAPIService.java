package seczkowski.mateusz.carservice.services;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;

import seczkowski.mateusz.carservice.exceptions.DownloadException;
import seczkowski.mateusz.carservice.interfaces.IGetUserAPICallback;
import seczkowski.mateusz.carservice.model.User;

public class GetUserAPIService extends ApiService {

    IGetUserAPICallback callback;
    private String login;
    private Exception error;

    public GetUserAPIService(IGetUserAPICallback callback, Context context) {
        super(context);
        this.callback = callback;
    }

    public void getUserDataByLogin(String login) {

        this.login = login;

        new AsyncTask<String, Void, String>() {

            @Override
            protected String doInBackground(String... strings) {
                try {
                    URL url = new URL(String.format(API_BaseURL() + "api/rest/users?userLogin=%s", strings[0]));
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    return oAuthAuthorizeSendRequestAndGetResult(connection, null, null);
                } catch (Exception e) {
                    error = e;
                }

                return null;
            }

            @Override
            protected void onPostExecute(String s) {

                if (s == null && error != null) {
                    callback.getUserDataServiceFail(error);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(s);

                    if (object.has("message")) {
                        callback.getUserDataServiceFail(new DownloadException(object.optString("message")));
                        return;
                    }

                    User user = new User();
                    user.populate(object);
                    callback.getUserDataServiceSuccess(user);

                } catch (JSONException e) {
                    callback.getUserDataServiceFail(e);
                }
            }
        }.execute(this.login);
    }
}
