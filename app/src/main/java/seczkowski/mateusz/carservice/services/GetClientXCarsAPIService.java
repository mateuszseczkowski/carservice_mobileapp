package seczkowski.mateusz.carservice.services;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;

import seczkowski.mateusz.carservice.exceptions.DownloadException;
import seczkowski.mateusz.carservice.interfaces.IGetClientXCarsAPICallback;
import seczkowski.mateusz.carservice.model.ClientXCar;

public class GetClientXCarsAPIService extends ApiService {

    IGetClientXCarsAPICallback callback;
    private int userId;
    private Exception error;

    public GetClientXCarsAPIService(IGetClientXCarsAPICallback callback, Context context) {
        super(context);
        this.callback = callback;
    }

    public void downloadClientXCarsCarsByUserId(int userId) {

        this.userId = userId;

        new AsyncTask<Integer, Void, String>() {

            @Override
            protected String doInBackground(Integer... integers) {
                try {
                    URL url = new URL(String.format(API_BaseURL() + "api/rest/clientxcars?userId=%s", integers[0]));
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    return oAuthAuthorizeSendRequestAndGetResult(connection, null, null);
                } catch (Exception e) {
                    error = e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {

                if (s == null && error != null) {
                    callback.getClientsCarsListServiceFail(error);
                    return;
                }

                try {
                    JSONObject all = new JSONObject(s);

                    if (all.has("message")) {
                        callback.getClientsCarsListServiceFail(new DownloadException(all.optString("message")));
                        return;
                    }

                    int count = all.optInt("count");
                    if (count == 0) {
                        callback.getClientsCarsListServiceFail(new DownloadException("No data"));
                        return;
                    }

                    ClientXCar clientXCar = new ClientXCar();
                    clientXCar.setCount(all.optInt("count"));
                    clientXCar.populate(all.optJSONArray("clientXCars"));
                    callback.getClientsCarsListServiceSuccess(clientXCar);

                } catch (JSONException e) {
                    callback.getClientsCarsListServiceFail(e);
                }
            }
        }.execute(this.userId);
    }
}
