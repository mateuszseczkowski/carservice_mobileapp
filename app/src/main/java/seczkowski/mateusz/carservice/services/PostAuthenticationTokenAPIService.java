package seczkowski.mateusz.carservice.services;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;

import seczkowski.mateusz.carservice.exceptions.DownloadException;
import seczkowski.mateusz.carservice.interfaces.IPostAuthenticationTokenAPICallback;

public class PostAuthenticationTokenAPIService extends ApiService {

    IPostAuthenticationTokenAPICallback callback;
    private Exception error;

    public PostAuthenticationTokenAPIService(IPostAuthenticationTokenAPICallback callback, Context context) {
        super(context);
        this.callback = callback;
    }

    public void getOAuthToken(final String login, final String password) {

        new AsyncTask<Integer, Void, String>() {

            @Override
            protected String doInBackground(Integer... integers) {
                try {
                    URL url = new URL(String.format(API_BaseURL() + "/Oauth/Token"));

                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    return oAuthAuthorizeSendRequestAndGetResult(connection, "username=" + login
                            + "&password=" + password
                            + "&grant_type=" + "password", "POST");

                } catch (Exception e) {
                    error = e;
                }

                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                if (s == null && error != null) {
                    callback.authenticationServiceFail(error);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(s);

                    if (object.has("error")) {
                        callback.authenticationServiceFail(new DownloadException(object.optString("error")));
                        return;
                    }

                    callback.authenticationServiceSuccess(object.getString("access_token"));

                } catch (JSONException e) {
                    callback.authenticationServiceFail(e);
                }
            }
        }.execute();
    }
}
