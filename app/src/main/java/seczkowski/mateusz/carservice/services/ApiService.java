package seczkowski.mateusz.carservice.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import seczkowski.mateusz.carservice.R;

public class ApiService {
    private SharedPreferences settings;
    private String API_SERVICE_DEBUG = "http://10.1.128.34:15735/"; //localhost from AVD
    private String API_SERVICE_DEBUG2 = "http://192.168.1.5:15735/"; //localhost from AVD
    private String API_SERVICE_PRODUCTION = "https://carservice.dmcs.pl/";

    private String token;

    public ApiService(Context context) {
        settings = context.getSharedPreferences(context.getString(R.string.KEY_CARSERVICE_APP_PREF), 0);

        this.token = settings.getString(context.getString(R.string.KEY_OAUTH_TOKEN), "");
    }

    public String API_BaseURL() {
        return API_SERVICE_DEBUG2;
    }

    private HttpURLConnection addOAuthRequestProperties(HttpURLConnection connection) {
        connection.addRequestProperty("Accept", "application/json");
        connection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        connection.addRequestProperty("Authorization", "bearer " + token);

        return connection;
    }

    protected String oAuthAuthorizeSendRequestAndGetResult(HttpURLConnection connection, String writeBytes, String requestMethod) throws IOException {
        connection = addOAuthRequestProperties(connection);

        connection.setReadTimeout(19000);
        connection.setConnectTimeout(19000);

        if (writeBytes != null && requestMethod.equals("POST")) {
            connection.setRequestMethod(requestMethod);
            connection.setDoOutput(true);

            DataOutputStream dos = new DataOutputStream(connection.getOutputStream());

            dos.writeBytes(writeBytes);

            dos.flush();
            dos.close();
        }
        else if (requestMethod != null) {
            connection.setRequestMethod(requestMethod);
            connection.connect();
        }
        else {
            connection.connect();
        }

        int responseCode = connection.getResponseCode();
        InputStream inputStream;

        if (responseCode >= 400 && responseCode <= 499)
            inputStream = connection.getErrorStream();
        else
            inputStream = connection.getInputStream();

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder result = new StringBuilder();
        String line;
        while ((line=bufferedReader.readLine())!=null) {
            result.append(line);
        }

        Log.i("ARRAY", result.toString());
        return result.toString();
    }
}