package seczkowski.mateusz.carservice.services;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;

import seczkowski.mateusz.carservice.exceptions.DownloadException;
import seczkowski.mateusz.carservice.interfaces.IGetIncomingRepairsListAPICallback;
import seczkowski.mateusz.carservice.model.IncomingRepairsList;

public class GetIncomingRepairsListAPIService extends ApiService {

    IGetIncomingRepairsListAPICallback callback;
    private int mechanicId;
    private Exception error;

    public GetIncomingRepairsListAPIService(IGetIncomingRepairsListAPICallback callback, Context context) {
        super(context);
        this.callback = callback;
    }

    public void downloadIncomingRepairsListOfMechanic(int mechanicId) {

        this.mechanicId = mechanicId;

        new AsyncTask<Integer, Void, String>() {

            @Override
            protected String doInBackground(Integer... integers) {
                try {
                    URL url = new URL(String.format(API_BaseURL() + "api/rest/incomingrepairs?mechanicId=%s", integers[0]));
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    return oAuthAuthorizeSendRequestAndGetResult(connection, null, null);
                } catch (Exception e) {
                    error = e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                if (s == null && error != null) {
                    callback.serviceFail(error);
                    return;
                }

                try {
                    JSONObject all = new JSONObject(s);

                    if (all.has("message")) {
                        callback.serviceFail(new DownloadException(all.optString("message")));
                        return;
                    }

                    int count = all.optInt("count");
                    if (count == 0) {
                        callback.serviceFail(new DownloadException("No data"));
                        return;
                    }

                    IncomingRepairsList incomingRepairsList = new IncomingRepairsList();
                    incomingRepairsList.setCount(all.optInt("count"));
                    incomingRepairsList.populate(all.optJSONArray("incomingRepairs"));
                    callback.getIncomingRepairsServiceSuccess(incomingRepairsList);
                } catch (JSONException e) {
                    callback.serviceFail(e);
                }
            }
        }.execute(this.mechanicId);
    }
}
