package seczkowski.mateusz.carservice.services;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;

import seczkowski.mateusz.carservice.exceptions.DownloadException;
import seczkowski.mateusz.carservice.interfaces.IPutPersonalUserDataAPICallback;
import seczkowski.mateusz.carservice.model.User;

public class PutPersonalUserDataAPIService extends ApiService {

    IPutPersonalUserDataAPICallback callback;
    User user;
    int responseCode;
    private Exception error;

    public PutPersonalUserDataAPIService(IPutPersonalUserDataAPICallback callback, Context context) {
        super(context);
        this.callback = callback;
    }

    public void updateUserPersonalData(User user) {
        this.user = user;

        new AsyncTask<User, Void, String>() {

            @Override
            protected String doInBackground(User... users) {
                try {
                    URL url = new URL(String.format(API_BaseURL() + "api/rest/users/%s?n=%s&sn=%s",
                            users[0].getId(), users[0].getName(), users[0].getSurname()));

                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    return oAuthAuthorizeSendRequestAndGetResult(connection, null, "PUT");
                } catch (Exception e) {
                    error = e;
                }

                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                if (s == null && error != null) {
                    callback.updateUserDataServiceFail(error);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(s);

                    if (object.has("message")) {
                        callback.updateUserDataServiceFail(new DownloadException(object.optString("message")));
                        return;
                    }

                    User user = new User();
                    user.populate(object);

                    callback.updateUserDataServiceSuccess(user);

                } catch (JSONException e) {
                    callback.updateUserDataServiceFail(e);
                }
            }
        }.execute(user);
    }
}
