package seczkowski.mateusz.carservice.services;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;

import seczkowski.mateusz.carservice.exceptions.DownloadException;
import seczkowski.mateusz.carservice.interfaces.IGetMechanicsAPICallback;
import seczkowski.mateusz.carservice.model.Mechanic;

public class GetMechanicAPIService extends ApiService {

    IGetMechanicsAPICallback callback;
    private Exception error;

    public GetMechanicAPIService(IGetMechanicsAPICallback callback, Context context) {
        super(context);
        this.callback = callback;
    }

    public void downloadAllMechanics() {

        new AsyncTask<String, Void, String>() {

            @Override
            protected String doInBackground(String... strings) {
                try {
                    URL url = new URL(API_BaseURL() + "api/rest/mechanics");
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    return oAuthAuthorizeSendRequestAndGetResult(connection, null, null);
                } catch (Exception e) {
                    error = e;
                }

                return null;
            }

            @Override
            protected void onPostExecute(String s) {

                if (s == null && error != null) {
                    callback.getMechanicsServiceFail(error);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(s);

                    if (object.has("message")) {
                        callback.getMechanicsServiceFail(new DownloadException(object.optString("message")));
                        return;
                    }

                    int count = object.optInt("count");
                    if (count == 0) {
                        callback.getMechanicsServiceFail(new DownloadException("No data"));
                        return;
                    }

                    Mechanic mechanic = new Mechanic();
                    mechanic.setCount(object.optInt("count"));
                    mechanic.populate(object.optJSONArray("mechanics"));
                    callback.getMechanicsServiceSuccess(mechanic);

                } catch (JSONException e) {
                    callback.getMechanicsServiceFail(e);
                }
            }
        }.execute();
    }
}
