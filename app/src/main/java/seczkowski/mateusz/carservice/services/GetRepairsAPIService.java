package seczkowski.mateusz.carservice.services;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;

import seczkowski.mateusz.carservice.exceptions.DownloadException;
import seczkowski.mateusz.carservice.interfaces.IGetRepairAPICallback;
import seczkowski.mateusz.carservice.interfaces.IGetRepairsAPICallback;
import seczkowski.mateusz.carservice.model.Repair;
import seczkowski.mateusz.carservice.model.Repairs;

public class GetRepairsAPIService extends ApiService {
    IGetRepairsAPICallback callbackRepairs;
    IGetRepairAPICallback callbackRepair;

    private int userId;
    private int repairId;
    private Exception error;

    public GetRepairsAPIService(IGetRepairsAPICallback callback, Context context) {
        super(context);
        this.callbackRepairs = callback;
    }

    public GetRepairsAPIService(IGetRepairAPICallback callbackRepair, Context context) {
        super(context);
        this.callbackRepair = callbackRepair;
    }

    public void downloadRepairsListOfUser(int userId) {

        this.userId = userId;

        new AsyncTask<Integer, Void, String>() {

            @Override
            protected String doInBackground(Integer... integers) {
                try {
                    URL url = new URL(String.format(API_BaseURL() + "api/rest/repairs?userId=%s", integers[0]));

                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    return oAuthAuthorizeSendRequestAndGetResult(connection, null, null);
                } catch (Exception e) {
                    error = e;
                }

                return null;
            }

            @Override
            protected void onPostExecute(String s) {

                if (s == null && error != null) {
                    callbackRepairs.serviceFail(error);
                    return;
                }
                try {
                    JSONObject all = new JSONObject(s);

                    if (all.has("message")) {
                        callbackRepairs.serviceFail(new DownloadException(all.optString("message")));
                        return;
                    }

                    int count = all.optInt("count");
                    if (count == 0) {
                        callbackRepairs.serviceFail(new DownloadException("No data"));
                        return;
                    }

                    Repairs repairs = new Repairs();
                    repairs.setCount(all.optInt("count"));
                    repairs.populate(all.optJSONArray("repairs"));
                    callbackRepairs.serviceSuccess(repairs);

                } catch (JSONException e) {
                    callbackRepairs.serviceFail(e);
                }
            }
        }.execute(this.userId);
    }

    public void downloadRepairById(final int id) {
        this.repairId = id;

        new AsyncTask<Integer, Void, String>() {

            @Override
            protected String doInBackground(Integer... integers) {
                try {
                    URL url = new URL(String.format(API_BaseURL() + "api/rest/repairs/%s", id));
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    return oAuthAuthorizeSendRequestAndGetResult(connection, null, null);

                } catch (Exception e) {
                    error = e;
                }

                return null;
            }

            @Override
            protected void onPostExecute(String s) {

                if (s == null && error != null) {
                    callbackRepair.serviceFail(error);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(s);

                    if (object.has("message")) {
                        callbackRepair.serviceFail(new DownloadException(object.optString("message")));
                        return;
                    }

                    Repair repair = new Repair();
                    repair.populate(object);
                    callbackRepair.serviceSuccess(repair);

                } catch (JSONException e) {
                    callbackRepair.serviceFail(e);
                }
            }
        }.execute(this.repairId);
    }
}
