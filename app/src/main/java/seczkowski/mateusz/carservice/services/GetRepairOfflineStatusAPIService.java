package seczkowski.mateusz.carservice.services;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;

import seczkowski.mateusz.carservice.exceptions.DownloadException;
import seczkowski.mateusz.carservice.interfaces.IGetRepairOfflineStatusAPICallback;
import seczkowski.mateusz.carservice.model.RepairOfflineStatus;

public class GetRepairOfflineStatusAPIService extends ApiService {

    IGetRepairOfflineStatusAPICallback callback;
    private String plateNo, repairHash;
    private Exception error;

    public GetRepairOfflineStatusAPIService(IGetRepairOfflineStatusAPICallback callback, Context context) {
        super(context);
        this.callback = callback;
    }

    public void getRepairStatusWithoutLoginByHashAndPlateNo(final String plateNo, final String repairHash) {
        this.plateNo = plateNo;
        this.repairHash = repairHash;

        new AsyncTask<String, Void, String>() {

            @Override
            protected String doInBackground(String... strings) {
                try {
                    URL url = new URL(String.format(API_BaseURL() + "api/rest/repairstatus?plateNo=%s&repairHash=%s", plateNo, repairHash));
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    return oAuthAuthorizeSendRequestAndGetResult(connection, null, null);
                } catch (Exception e) {
                    error = e;
                }

                return null;
            }

            @Override
            protected void onPostExecute(String s) {

                if (s == null && error != null) {
                    callback.checkRepairStatusServiceFail(error);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(s);

                    if (object.has("message")) {
                        callback.checkRepairStatusServiceFail(new DownloadException(object.optString("message")));
                        return;
                    }

                    RepairOfflineStatus repairOfflineStatus = new RepairOfflineStatus();
                    repairOfflineStatus.populate(object);
                    callback.checkRepairStatusServiceSuccess(repairOfflineStatus);

                } catch (JSONException e) {
                    callback.checkRepairStatusServiceFail(e);
                }
            }
        }.execute(this.plateNo, this.repairHash);
    }
}
