package seczkowski.mateusz.carservice.services;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;

import seczkowski.mateusz.carservice.exceptions.DownloadException;
import seczkowski.mateusz.carservice.interfaces.IPostClientXCarAPICallback;
import seczkowski.mateusz.carservice.model.ClientXCar;

public class PostClientXCarAPIService extends ApiService {

    IPostClientXCarAPICallback callback;
    int userId;
    private Exception error;

    public PostClientXCarAPIService(IPostClientXCarAPICallback callback, Context context) {
        super(context);
        this.callback = callback;
    }

    public void addClientXCar(final ClientXCar clientXCar, int userId) {

        this.userId = userId;

        new AsyncTask<Integer, Void, String>() {

            @Override
            protected String doInBackground(Integer... integers) {
                try {
                    URL url = new URL(String.format(API_BaseURL() + "api/rest/clientxcars?userId=%s&plateNo=%s", integers[0], clientXCar.getPlateNo()));

                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    return oAuthAuthorizeSendRequestAndGetResult(connection, "model=" + clientXCar.getCar().getModel()
                            + "&make=" + clientXCar.getCar().getMake()
                            + "&year=" + clientXCar.getCar().getYear()
                            + "&capacity=" + clientXCar.getCar().getCapacity()
                            + "&bhp=" + clientXCar.getCar().getBhp()
                            + "&version=" + clientXCar.getCar().getVersion()
                            + "&vinNo=" + clientXCar.getCar().getVinNo(), "POST");

                } catch (Exception e) {
                    error = e;
                }

                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                if (s == null && error != null) {
                    callback.serviceFail(error);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(s);

                    if (object.has("message")) {
                        callback.serviceFail(new DownloadException(object.optString("message")));
                        return;
                    }

                    callback.serviceSuccess(object.getInt("clientXCarID"));

                } catch (JSONException e) {
                    callback.serviceFail(e);
                }
            }
        }.execute(this.userId);
    }
}
