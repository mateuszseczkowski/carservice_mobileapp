package seczkowski.mateusz.carservice.services;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;

import seczkowski.mateusz.carservice.exceptions.DownloadException;
import seczkowski.mateusz.carservice.interfaces.IGetInvoicesListAPICallback;
import seczkowski.mateusz.carservice.model.InvoicesList;

public class GetInvoicesListAPIService extends ApiService {

    IGetInvoicesListAPICallback callback;
    private int userId;
    private Exception error;

    public GetInvoicesListAPIService(IGetInvoicesListAPICallback callback, Context context) {
        super(context);
        this.callback = callback;
    }

    public void downloadInvoicesListData(int userId) {

        this.userId = userId;

        new AsyncTask<Integer, Void, String>() {

            @Override
            protected String doInBackground(Integer... integers) {
                try {
                    URL url = new URL(String.format(API_BaseURL() + "api/rest/invoices?userId=%s", integers[0]));
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    return oAuthAuthorizeSendRequestAndGetResult(connection, null, null);
                } catch (Exception e) {
                    error = e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                if (s == null && error != null) {
                    callback.serviceFail(error);
                    return;
                }

                try {
                    JSONObject all = new JSONObject(s);

                    if (all.has("message")) {
                        callback.serviceFail(new DownloadException(all.optString("message")));
                        return;
                    }

                    int count = all.optInt("count");
                    if (count == 0) {
                        callback.serviceFail(new DownloadException("No data"));
                        return;
                    }

                    InvoicesList invoices = new InvoicesList();
                    invoices.populate(all.optJSONArray("invoices"));
                    invoices.setCount(all.optInt("count"));
                    callback.getInvoicesServiceSuccess(invoices);
                } catch (JSONException e) {
                    callback.serviceFail(e);
                }
            }
        }.execute(this.userId);
    }
}
