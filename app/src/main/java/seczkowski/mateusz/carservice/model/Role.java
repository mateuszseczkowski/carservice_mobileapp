package seczkowski.mateusz.carservice.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

import seczkowski.mateusz.carservice.interfaces.IJSONPopulator;

@SuppressWarnings("serial")
public class Role implements IJSONPopulator, Serializable {
    private int Id;
    private String Name;

    public int getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public Role() {
    }

    public Role(int id) {
        Id = id;
    }

    @Override
    public void populate(JSONObject jObject) {
        Id = jObject.optInt("id");
        Name = jObject.optString("name");
    }

    @Override
    public void populate(JSONArray jArray) {
    }
}
