package seczkowski.mateusz.carservice.model;

import com.j256.ormlite.field.DatabaseField;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import seczkowski.mateusz.carservice.interfaces.IJSONPopulator;

public class ClientXCar implements IJSONPopulator {
    private int ClientXCarId;
    private String PlateNo;
    private Car Car;
    private boolean IsActive;
    private boolean IsDefault;
    private List<ClientXCar> ClientXCars = new ArrayList<>();
    private Integer Count;

    public ClientXCar() {}

    public ClientXCar(String plateNo, Car car) {
        PlateNo = plateNo;
        Car = car;
    }

    public int getClientXCarId() {
        return ClientXCarId;
    }

    public String getPlateNo() {
        return PlateNo;
    }

    public seczkowski.mateusz.carservice.model.Car getCar() {
        return Car;
    }

    public List<ClientXCar> getClientXCars() {
        return ClientXCars;
    }

    public Integer getCount() {
        return Count;
    }

    public void setCount(Integer count) {
        Count = count;
    }

    public String getClientXCarBasicData() {
        return PlateNo + " " + Car.getCarBasicData();
    }

    public boolean isActive() {
        return IsActive;
    }

    public boolean isDefault() {
        return IsDefault;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public void setDefault(boolean aDefault) {
        IsDefault = aDefault;
    }

    @Override
    public String toString() {
        return getClientXCarBasicData();
    }

    @Override
    public void populate(JSONObject jObject) {
        ClientXCarId = jObject.optInt("clientXCarID");
        PlateNo = jObject.optString("plateNo");
        Car = new Car(); Car.populate(jObject.optJSONObject("car"));
        IsActive = jObject.optBoolean("isActive");
        IsDefault = jObject.optBoolean("isDefault");
    }

    @Override
    public void populate(JSONArray jArray) {

        for (int i=0; i < jArray.length(); i++)
        {
            ClientXCar clientXCar = new ClientXCar();
            try {
                clientXCar.populate(jArray.getJSONObject(i));
                ClientXCars.add(clientXCar);
            } catch (JSONException e) {
                // Oops
            }
        }
    }
}