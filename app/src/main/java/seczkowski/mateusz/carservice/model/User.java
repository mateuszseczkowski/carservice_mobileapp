package seczkowski.mateusz.carservice.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

import seczkowski.mateusz.carservice.interfaces.IJSONPopulator;

@SuppressWarnings("serial")
public class User implements IJSONPopulator, Serializable {
    private int Id;
    private String Name;
    private String Surname;
    private String Email;
    private String Phone;
    private String Address;
    private String City;
    private Role Role;
    private boolean IsMechanic;
    private Integer MechanicId;

    public int getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public String getNameAndSurname() {
        return Name + " " + Surname;
    }

    public String getEmail() {
        return Email;
    }

    public String getPhone() {
        return Phone;
    }

    public boolean isMechanic() {
        return Role.getId() == 2;
    }

    public Integer getMechanicId() {
        return MechanicId;
    }

    public User() {
    }

    public User(Role role) {
        Role = role;
    }

    @Override
    public void populate(JSONObject jObject) {
        Id = jObject.optInt("id");
        Name = jObject.optString("name");
        Surname = jObject.optString("surname");
        Email = jObject.optString("email");
        Phone = jObject.optString("phone");
        //... more if needed
        Role = new Role(); Role.populate(jObject.optJSONObject("role"));
        MechanicId = jObject.optInt("mechanicId");
    }

    @Override
    public void populate(JSONArray jArray) {
    }
}
