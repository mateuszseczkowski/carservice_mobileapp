package seczkowski.mateusz.carservice.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import seczkowski.mateusz.carservice.interfaces.IJSONPopulator;

public class InvoicesList implements IJSONPopulator {
    private int Count;
    private List<Invoice> Invoices = new ArrayList<>();

    public void setCount(int count) {
        Count = count;
    }

    public int getCount() {
        return Count;
    }

    public List<Invoice> getInvoices() {
        return Invoices;
    }

    @Override
    public void populate(JSONObject jObject) {
    }

    @Override
    public void populate(JSONArray jArray) {
        for (int i=0; i < jArray.length(); i++)
        {
            Invoice invoice = new Invoice();
            try {
                invoice.populate(jArray.getJSONObject(i));
                Invoices.add(invoice);
            } catch (JSONException e) {
                // Oops
            }
        }
    }
}
