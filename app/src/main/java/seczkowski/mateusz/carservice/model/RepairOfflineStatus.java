package seczkowski.mateusz.carservice.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Date;

import seczkowski.mateusz.carservice.helpers.DateParserFormatterHelper;
import seczkowski.mateusz.carservice.interfaces.IJSONPopulator;

public class RepairOfflineStatus implements IJSONPopulator {
    private String RepairHash;
    private Date StartDate;
    private Date RequestDate;
    private Date PlannedEndDate;
    private boolean IsFinished;
    private String Description;
    private Mechanic Mechanic;
    private ClientXCar ClientXCar;

    public String getRepairHash() {
        return RepairHash;
    }

    public Date getPlannedEndDate() {
        return PlannedEndDate;
    }

    public boolean isFinished() {
        return IsFinished;
    }

    public String getDescription() {
        return Description;
    }

    public Mechanic getMechanic() {
        return Mechanic;
    }

    public seczkowski.mateusz.carservice.model.ClientXCar getClientXCar() {
        return ClientXCar;
    }

    @Override
    public void populate(JSONObject jObject) {
        IsFinished = jObject.optBoolean("isFinished");
        RepairHash = jObject.optString("repairHash");
        try {
            StartDate = DateParserFormatterHelper.ParseDateFromString(jObject.optString("startDate"));
            RequestDate = DateParserFormatterHelper.ParseDateFromString(jObject.optString("requestDate"));
            PlannedEndDate = DateParserFormatterHelper.ParseDateFromString(jObject.optString("plannedEndDate"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Description = jObject.optString("description");
        Mechanic = new Mechanic(); Mechanic.populate(jObject.optJSONObject("mechanic"));
        ClientXCar = new ClientXCar(); ClientXCar.populate(jObject.optJSONObject("clientXCar"));
    }

    @Override
    public void populate(JSONArray jArray) {
    }
}
