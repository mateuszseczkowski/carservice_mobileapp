package seczkowski.mateusz.carservice.model;

import org.json.JSONArray;
import org.json.JSONObject;

import seczkowski.mateusz.carservice.interfaces.IJSONPopulator;

public class Car implements IJSONPopulator { //TODO in whole model folder change the naming of the private properties to small first letter
    private Integer carId;
    private String model;
    private String make;
    private int year;
    private Double capacity;
    private Double bhp;
    private String version;
    private String vinNo;

    public Car() {}

    public Car(String model, String make, int year, Double capacity, Double bhp, String version, String vinNo, Boolean isActive) {
        this.model = model;
        this.make = make;
        this.year = year;
        this.capacity = capacity;
        this.bhp = bhp;
        this.version = version;
        this.vinNo = vinNo;
    }

    public int getCarId() {
        return carId;
    }

    public String getModel() {
        return model;
    }

    public String getMake() {
        return make;
    }

    public int getYear() {
        return year;
    }

    public Double getCapacity() {
        return capacity;
    }

    public Double getBhp() {
        return bhp;
    }

    public String getVersion() {
        return version;
    }

    public String getVinNo() {
        return vinNo;
    }

    public String getCarBasicData() {
        return make + " " + model + " " + year;
    }


    @Override
    public void populate(JSONObject jObject) {
        carId = jObject.optInt("carID");
        model = jObject.optString("model");
        make = jObject.optString("make");
        year = jObject.optInt("year");
        capacity = jObject.optDouble("capacity");
        bhp = jObject.optDouble("bhp");
        version = jObject.optString("version");
        vinNo = jObject.optString("vinNo");
    }

    @Override
    public void populate(JSONArray jArray) {

    }
}
