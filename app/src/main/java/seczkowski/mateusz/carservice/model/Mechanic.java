package seczkowski.mateusz.carservice.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import seczkowski.mateusz.carservice.interfaces.IJSONPopulator;

public class Mechanic implements IJSONPopulator {
    private int MechanicId;
    private User User;
    private List<Mechanic> MechanicsList = new ArrayList<>();
    private Integer Count;

    public int getMechanicId() {
        return MechanicId;
    }

    public User getUser() {
        return User;
    }

    public List<Mechanic> getMechanicsList() {
        return MechanicsList;
    }

    public Integer getCount() {
        return Count;
    }

    public void setCount(Integer count) {
        Count = count;
    }

    @Override
    public String toString() {
        return getUser().getNameAndSurname();
    }

    @Override
    public void populate(JSONObject jObject) {
        MechanicId = jObject.optInt("mechanicID");
        User = new User(); User.populate(jObject.optJSONObject("user"));
    }

    @Override
    public void populate(JSONArray jArray) {
        for (int i=0; i < jArray.length(); i++)
        {
            Mechanic mechanic = new Mechanic();
            try {
                mechanic.populate(jArray.getJSONObject(i));
                MechanicsList.add(mechanic);
            } catch (JSONException e) {
                // Oops
            }
        }
    }
}