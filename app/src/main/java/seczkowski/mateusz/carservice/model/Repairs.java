package seczkowski.mateusz.carservice.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import seczkowski.mateusz.carservice.interfaces.IJSONPopulator;

public class Repairs implements IJSONPopulator {
    private int Count;
    private List<Repair> Repairs = new ArrayList<>();

    public void setCount(int count) {
        Count = count;
    }

    public int getCount() {
        return Count;
    }

    public List<Repair> getRepairsList() {
        return Repairs;
    }

    @Override
    public void populate(JSONObject jObject) {
    }

    @Override
    public void populate(JSONArray jArray) {
        for (int i=0; i < jArray.length(); i++)
        {
            Repair repair = new Repair();
            try {
                repair.populate(jArray.getJSONObject(i));
                Repairs.add(repair);
            } catch (JSONException e) {
                // Oops
            }
        }
    }
}
