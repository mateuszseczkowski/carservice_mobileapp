package seczkowski.mateusz.carservice.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Date;

import seczkowski.mateusz.carservice.helpers.DateParserFormatterHelper;
import seczkowski.mateusz.carservice.interfaces.IJSONPopulator;

public class Invoice implements IJSONPopulator {
    private int InvoiceID;
    private int RepairID;
    private Date IssueDate;
    private Date DueDate;
    private double GrossAmount;
    private boolean IsFinished;

    public int getInvoiceID() {
        return InvoiceID;
    }

    public int getRepairID() {
        return RepairID;
    }

    public Date getIssueDate() {
        return IssueDate;
    }

    public double getGrossAmount() {
        return GrossAmount;
    }

    public Date getDueDate() {
        return DueDate;
    }

    public boolean isFinished() {
        return IsFinished;
    }

    @Override
    public void populate(JSONObject jObject) {
        InvoiceID = jObject.optInt("invoiceID");
        RepairID = jObject.optInt("repairID");
        try {
            IssueDate = DateParserFormatterHelper.ParseDateFromString(jObject.optString("issueDate"));
            DueDate = DateParserFormatterHelper.ParseDateFromString(jObject.optString("dueDate"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        GrossAmount = jObject.optDouble("grossAmount");
    }

    @Override
    public void populate(JSONArray jArray) {
    }
}
