package seczkowski.mateusz.carservice.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Date;

import seczkowski.mateusz.carservice.helpers.DateParserFormatterHelper;
import seczkowski.mateusz.carservice.interfaces.IJSONPopulator;

@DatabaseTable(tableName = "Repairs")
public class Repair implements IJSONPopulator {
    @DatabaseField(id = true)
    private int RepairID;

    @DatabaseField(canBeNull = true)
    private String RepairHash;

    @DatabaseField
    private Date StartDate;

    @DatabaseField
    private Date RequestDate;

    @DatabaseField
    private Date PlannedEndDate;

    @DatabaseField
    private boolean IsFinished;

    @DatabaseField
    private String Description;

    //@DatabaseField - if the offline object will require the ClientXCar field then the ClientXCar object will have to be extended to the Data Access Object similarly to this class
    //Also, the Child object must be parsed and put into the database before Parent
    private ClientXCar ClientXCar;

    public Repair() {} // non persisted class must have empty ctor

    public Repair(int repairID, String repairHash, Date startDate, Date requestDate, Date plannedEndDate, boolean isFinished, String description) { //The constructor of an object that is enough to be displayed on the repairs list view
        RepairID = repairID;
        RepairHash = repairHash;
        StartDate = startDate;
        RequestDate = requestDate;
        PlannedEndDate = plannedEndDate;
        IsFinished = isFinished;
        Description = description;
    }

    public int getRepairID() {
        return RepairID;
    }

    public String getRepairHash() {
        return RepairHash;
    }

    public Date getStartDate() {
        return StartDate;
    }

    public Date getRequestDate() {
        return RequestDate;
    }

    public Date getPlannedEndDate() {
        return PlannedEndDate;
    }

    public boolean isFinished() {
        return IsFinished;
    }

    public String getDescription() {
        return Description;
    }

    public String toFilterString() { //The idea of searching after all the data came from the jQuery's Datatables searching method
        return RepairHash + " " + RepairID + " " + StartDate + " " + RequestDate
                + " " + PlannedEndDate + " " + IsFinished + " " + Description;
    }

    public ClientXCar getClientXCar() {
        return ClientXCar;
    }

    @Override
    public void populate(JSONObject jObject) {
        RepairID = jObject.optInt("repairID");
        RepairHash = jObject.optString("repairHash");
        IsFinished = jObject.optBoolean("isFinished");
        try {
            StartDate = DateParserFormatterHelper.ParseDateFromString(jObject.optString("startDate"));
            RequestDate = DateParserFormatterHelper.ParseDateFromString(jObject.optString("requestDate"));
            PlannedEndDate = DateParserFormatterHelper.ParseDateFromString(jObject.optString("plannedEndDate"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Description = jObject.optString("description");
        ClientXCar = new ClientXCar(); ClientXCar.populate(jObject.optJSONObject("clientXCar"));
    }

    @Override
    public void populate(JSONArray jArray) {
    }
}
