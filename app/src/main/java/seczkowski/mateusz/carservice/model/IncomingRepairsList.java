package seczkowski.mateusz.carservice.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import seczkowski.mateusz.carservice.interfaces.IJSONPopulator;

public class IncomingRepairsList implements IJSONPopulator {
    private int Count;
    private List<IncomingRepair> IncomingRepairs = new ArrayList<>();

    public void setCount(int count) {
        Count = count;
    }

    public int getCount() {
        return Count;
    }

    public List<IncomingRepair> getIncomingRepairs() {
        return IncomingRepairs;
    }

    @Override
    public void populate(JSONObject jObject) {
    }

    @Override
    public void populate(JSONArray jArray) {
        for (int i=0; i < jArray.length(); i++)
        {
            IncomingRepair incomingRepair = new IncomingRepair();
            try {
                incomingRepair.populate(jArray.getJSONObject(i));
                IncomingRepairs.add(incomingRepair);
            } catch (JSONException e) {
                // Oops
            }
        }
    }
}
