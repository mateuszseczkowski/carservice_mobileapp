package seczkowski.mateusz.carservice.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Date;
import java.util.StringTokenizer;

import seczkowski.mateusz.carservice.helpers.DateParserFormatterHelper;
import seczkowski.mateusz.carservice.interfaces.IJSONPopulator;

public class IncomingRepair implements IJSONPopulator {
    private int Id;
    private int Status;
    private int ClientId;
    private int MechanicId;
    private int ClientXCarId;
    private Date IncomingRepairDateTime;
    private String MessageFromClient;

    public int getId() {
        return Id;
    }

    public int getStatus() {
        return Status;
    }

    public int getClientId() {
        return ClientId;
    }

    public int getMechanicId() {
        return MechanicId;
    }

    public int getClientXCarId() {
        return ClientXCarId;
    }

    public Date getIncomingRepairDateTime() {
        return IncomingRepairDateTime;
    }

    public String getMessageFromClient() {
        return MessageFromClient;
    }

    @Override
    public void populate(JSONObject jObject) {
        Id = jObject.optInt("id");
        Status = jObject.optInt("status");
        try {
            IncomingRepairDateTime = DateParserFormatterHelper.ParseDateFromString(jObject.optString("incomingRepairDateTime"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        MessageFromClient = jObject.optString("messageFromClient");
    }

    @Override
    public void populate(JSONArray jArray) {
    }
}
