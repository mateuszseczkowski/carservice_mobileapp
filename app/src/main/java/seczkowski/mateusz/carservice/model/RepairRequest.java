package seczkowski.mateusz.carservice.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Date;

import seczkowski.mateusz.carservice.helpers.DateParserFormatterHelper;
import seczkowski.mateusz.carservice.interfaces.IJSONPopulator;

public class RepairRequest implements IJSONPopulator {
    private Integer Id;
    private Integer Status;
    private Integer ClientId;
    private Integer MechanicId;
    private Integer ClientXCarId;
    private Date IncomingRepairDateTime;
    private String MessageFromClient;

    public RepairRequest() {
    }

    public RepairRequest(Integer mechanicId, Integer clientXCarId, String messageFromClient) {
        MechanicId = mechanicId;
        ClientXCarId = clientXCarId;
        MessageFromClient = messageFromClient;
    }

    public Integer getId() {
        return Id;
    }

    public Integer getStatus() {
        return Status;
    }

    public Integer getClientId() {
        return ClientId;
    }

    public Integer getMechanicId() {
        return MechanicId;
    }

    public Integer getClientXCarId() {
        return ClientXCarId;
    }

    public Date getIncomingRepairDateTime() {
        return IncomingRepairDateTime;
    }

    public String getMessageFromClient() {
        return MessageFromClient;
    }

    @Override
    public void populate(JSONObject jObject) {
        Id = jObject.optInt("id");
        Status = jObject.optInt("status");
        ClientId = jObject.optInt("clientId");
        MechanicId = jObject.optInt("mechanicId");
        ClientXCarId = jObject.optInt("clientXCarId");
        MessageFromClient = jObject.optString("messageFromClient");
        try {
            IncomingRepairDateTime = DateParserFormatterHelper.ParseDateFromString(jObject.optString("incomingRepairDateTime"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void populate(JSONArray jArray) {
    }
}
