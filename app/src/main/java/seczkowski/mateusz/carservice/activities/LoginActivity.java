package seczkowski.mateusz.carservice.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.facebook.stetho.Stetho;

import seczkowski.mateusz.carservice.R;
import seczkowski.mateusz.carservice.adapters.RepairStatusDialogAdapter;
import seczkowski.mateusz.carservice.exceptions.ServiceFailedHelper;
import seczkowski.mateusz.carservice.interfaces.IGetRepairOfflineStatusAPICallback;
import seczkowski.mateusz.carservice.interfaces.IGetUserAPICallback;
import seczkowski.mateusz.carservice.interfaces.IPostAuthenticationTokenAPICallback;
import seczkowski.mateusz.carservice.model.RepairOfflineStatus;
import seczkowski.mateusz.carservice.model.User;
import seczkowski.mateusz.carservice.services.GetRepairOfflineStatusAPIService;
import seczkowski.mateusz.carservice.services.GetUserAPIService;
import seczkowski.mateusz.carservice.services.PostAuthenticationTokenAPIService;

public class LoginActivity extends AppCompatActivity implements IPostAuthenticationTokenAPICallback, IGetUserAPICallback, IGetRepairOfflineStatusAPICallback {

    private TextInputEditText loginText, passwordText, repairHashText, plateNoText;
    private CheckBox rememberLoginCheckbox;
    private SharedPreferences settings;
    private PostAuthenticationTokenAPIService authenticateService;
    private GetUserAPIService getUserService;
    private GetRepairOfflineStatusAPIService getRepairStatusService;
    private ProgressDialog progressDialog;
    private Button loginButton, checkStatusButton;

    private String userLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Stetho.initializeWithDefaults(this); //http://facebook.github.io/stetho/
        setContentView(R.layout.activity_login);

        setCustomActionBar();

        settings = getSharedPreferences(getString(R.string.KEY_CARSERVICE_APP_PREF), 0);
        String token = settings.getString(getString(R.string.KEY_OAUTH_TOKEN), "");
        userLogin = settings.getString(getString(R.string.KEY_REMEMBERED_LOGIN), "");

        if (token.length() > 0 && userLogin.length() > 0) {
            getUserIdByLoginAndStartMainActivityOnSuccess(userLogin); //BSc explanation: this without "return" after invoke defends app for
                                                                    // the case that the user has been remembered on client side and deleted on the server side or the user Id somehow has been changed
        }
        ((TextView) findViewById(R.id.login_page_welcome_text))
                .setText(getString(R.string.welcome_row_1)
                        + "\n"
                        + getString(R.string.welcome_row_2));

        initializeView();
    }

    private void setCustomActionBar() {
        try {
            android.support.v7.app.ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayShowCustomEnabled(true);
            }
            LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);

            View v = inflater.inflate(R.layout.actionbar, null);

            if (actionBar != null) {
                actionBar.setCustomView(v);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            Log.e("", "setActionBar: " + "Could not set action bar image");
        }
    }

    private void initializeView() {
        loginButton = (Button) findViewById(R.id.loginpage_signin);
        loginText = (TextInputEditText) findViewById(R.id.login_page_login);
        passwordText = (TextInputEditText) findViewById(R.id.login_page_password);
        rememberLoginCheckbox = (CheckBox) findViewById(R.id.login_page_remember);
        loginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                userLogin = loginText.getText().toString();
                String password_ = passwordText.getText().toString();
                if (isCorrectData(userLogin, password_)) {
                    try{
                        authenticate(userLogin, password_);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                        new AlertDialog.Builder(LoginActivity.this)
                                .setMessage(getResources().getString(R.string.error_message_authenticationn)).setPositiveButton("OK", null).show();
                    }
                }
            }
        });
        repairHashText = (TextInputEditText) findViewById(R.id.loginpage_repair_hash);
        plateNoText = (TextInputEditText) findViewById(R.id.loginpage_plate_number);
        checkStatusButton = (Button) findViewById(R.id.loginpage_check_repair_status_withoutlogin);
        checkStatusButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getRepairStatus(plateNoText.getText().toString(), repairHashText.getText().toString());
            }
        });
    }

    private boolean isCorrectData(String l, String p) {
        // TO DO
        if (l.length() > 0 && p.length() > 0) {
            return true;
        }
        return false;
    }

    private void authenticate(String login, String password) {
        authenticateService = new PostAuthenticationTokenAPIService(this, this);
        authenticateService.getOAuthToken(login, password);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.progress_log_in));
        progressDialog.show();
    }

    @Override
    public void authenticationServiceSuccess(String accessToken) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(getString(R.string.KEY_OAUTH_TOKEN), accessToken);
        editor.apply();

        progressDialog.hide();

        getUserIdByLoginAndStartMainActivityOnSuccess(userLogin);
    }

    private void getUserIdByLoginAndStartMainActivityOnSuccess(String login) {
        getUserService = new GetUserAPIService(this, this);
        getUserService.getUserDataByLogin(login);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.progress_loading_data));
        progressDialog.show();
    }

    @Override
    public void getUserDataServiceSuccess(User user) {
        SharedPreferences.Editor editor = settings.edit();
        if (rememberLoginCheckbox.isChecked()) {
            editor.putString(getString(R.string.KEY_REMEMBERED_LOGIN), user.getEmail());
        }
        editor.putString(getString(R.string.KEY_CURRENT_USER_LOGIN), user.getEmail());
        editor.apply(); //apply instead of commit saves data in background and commit immediately

        progressDialog.hide();
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        i.putExtra(getString(R.string.KEY_USER), user);
        startActivity(i);
        finish();
    }

    private void getRepairStatus(String plateNo, String repairHash) {
        getRepairStatusService = new GetRepairOfflineStatusAPIService(this, this);
        getRepairStatusService.getRepairStatusWithoutLoginByHashAndPlateNo(plateNo, repairHash);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.progress_loading_repair_status));
        progressDialog.show();
    }

    @Override
    public void checkRepairStatusServiceSuccess(RepairOfflineStatus repairOfflineStatus) {
        RepairStatusDialogAdapter repairStatusDialogAdapter = new RepairStatusDialogAdapter(this);
        LayoutInflater layoutInflater = getLayoutInflater();
        View dialogLayout = layoutInflater.inflate(R.layout.dialog_repair_status, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setView(dialogLayout);
        AlertDialog dialog = builder.create();
        dialog.show();

        repairStatusDialogAdapter.initView(dialogLayout, this, dialog);
        repairStatusDialogAdapter.setData(repairOfflineStatus);

        progressDialog.cancel();
    }

    @Override
    public void authenticationServiceFail(Exception exception) {
        progressDialog.hide();
        new ServiceFailedHelper(exception, getString(R.string.constraints_authentication), LoginActivity.this).showDialogOk();
    }

    @Override
    public void getUserDataServiceFail(Exception exception) {
        progressDialog.hide();
        new ServiceFailedHelper(exception, getString(R.string.constraints_authentication), LoginActivity.this).showDialogOk();
    }

    @Override
    public void checkRepairStatusServiceFail(Exception exception) {
        progressDialog.hide();
        new ServiceFailedHelper(exception, getString(R.string.error_message_rep_offline_status_dialog), LoginActivity.this).showDialogOk();
    }
}

