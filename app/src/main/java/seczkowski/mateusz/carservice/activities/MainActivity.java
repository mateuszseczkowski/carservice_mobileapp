package seczkowski.mateusz.carservice.activities;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import seczkowski.mateusz.carservice.R;
import seczkowski.mateusz.carservice.adapters.DrawerNavListAdapter;
import seczkowski.mateusz.carservice.fragments.AboutFragment;
import seczkowski.mateusz.carservice.fragments.IncomingRepairsFragment;
import seczkowski.mateusz.carservice.fragments.InvoicesFragment;
import seczkowski.mateusz.carservice.fragments.RepairsFragment;
import seczkowski.mateusz.carservice.fragments.SettingsFragment;
import seczkowski.mateusz.carservice.model.DrawerNavItem;
import seczkowski.mateusz.carservice.model.User;

import static android.support.v4.app.FragmentTransaction.*;

public class MainActivity extends MenuActivity {

    private User user;
    private DrawerLayout drawerLayout;
    private RelativeLayout drawerPanel;
    private ListView drawerNavListView;

    List<DrawerNavItem> drawerNavItems;
    List<Fragment> drawerNavFragments;

    ActionBarDrawerToggle actionBarDrawerToggle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

        user = ((User)getIntent().getSerializableExtra(getString(R.string.KEY_USER)));
        this.initDrawerPanel(user.isMechanic());

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void initDrawerPanel(boolean loggedUserIsMechanic) {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerPanel = (RelativeLayout) findViewById(R.id.drawer_panel);
        drawerNavListView = (ListView) findViewById(R.id.nav_list);

        drawerNavItems = new ArrayList<>();
        drawerNavItems.add(new DrawerNavItem("Active repairs", "Check your active repairs", R.drawable.ic_repairs_list));
        if (loggedUserIsMechanic) {
            drawerNavItems.add(new DrawerNavItem("Incoming repairs", "Check the incoming repairs", R.drawable.ic_compare_arrows_black_24dp));
            drawerNavItems.add(new DrawerNavItem("Invoices", "Manage your finances", R.drawable.ic_show_chart_black_24dp));
        }
        drawerNavItems.add(new DrawerNavItem("Settings", "", R.drawable.ic_settings));
        drawerNavItems.add(new DrawerNavItem("About", "", R.drawable.ic_about));

        DrawerNavListAdapter navListAdapter = new DrawerNavListAdapter(getApplicationContext(), R.layout.drawer_nav_item, drawerNavItems);
        drawerNavListView.setAdapter(navListAdapter);

        drawerNavFragments = new ArrayList<>();

        drawerNavFragments.add(new RepairsFragment());
        if (loggedUserIsMechanic) {
            drawerNavFragments.add(new IncomingRepairsFragment());
            drawerNavFragments.add(new InvoicesFragment());
        }
        drawerNavFragments.add(new SettingsFragment());
        drawerNavFragments.add(new AboutFragment());

        //load first as default
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager
                .beginTransaction()
                .replace(R.id.main_content, drawerNavFragments.get(0))
                .commit();

        setTitle(drawerNavItems.get(0).getTitle());
        drawerNavListView.setItemChecked(0, true);
        drawerLayout.closeDrawer(drawerPanel);

        //listener for nav list buttons
        drawerNavListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                FragmentManager	fragmentManager = getSupportFragmentManager();
                fragmentManager
                        .beginTransaction()
                        .setTransition(TRANSIT_FRAGMENT_FADE)
                        //.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right) //To be described in the thesis paper.
                        .replace(R.id.main_content, drawerNavFragments.get(position))
                        .commit();

                setTitle(drawerNavItems.get(position).getTitle());
                drawerNavListView.setItemChecked(position, true);
                drawerLayout.closeDrawer(drawerPanel);
            }
        });

        //listener for showing drawer panel
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                R.string.drawer_opened, R.string.drawer_closed) {
            @Override
            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                invalidateOptionsMenu();
                super.onDrawerClosed(drawerView);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {

            if (!drawerLayout.isDrawerOpen(drawerPanel)) {
                drawerLayout.openDrawer(drawerPanel);
            } else if (drawerLayout.isDrawerOpen(drawerPanel)) {
                drawerLayout.closeDrawer(drawerPanel);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
