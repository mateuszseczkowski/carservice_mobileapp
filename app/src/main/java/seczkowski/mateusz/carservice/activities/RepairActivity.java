package seczkowski.mateusz.carservice.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import seczkowski.mateusz.carservice.R;
import seczkowski.mateusz.carservice.adapters.RepairAdapter;
import seczkowski.mateusz.carservice.helpers.Data.DBHelper;
import seczkowski.mateusz.carservice.interfaces.IGetRepairAPICallback;
import seczkowski.mateusz.carservice.model.Repair;
import seczkowski.mateusz.carservice.services.GetRepairsAPIService;

public class RepairActivity extends MenuActivity implements IGetRepairAPICallback {
    GetRepairsAPIService service;
    ProgressDialog progressDialog;
    Bundle extras;
    int repairId;

    @Override
    public void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repair);

        extras = getIntent().getExtras();
        repairId = extras.getInt(getString(R.string.KEY_REPAIR_ID));

        getData(repairId);
    }

    private void getData(int repairId) {
        service = new GetRepairsAPIService(this, getApplicationContext());
        service.downloadRepairById(repairId);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading data...");
        progressDialog.show();
    }

    @Override
    public void serviceSuccess(Repair repair) {
        progressDialog.hide();
        setRepairData(repair);
    }

    private void setRepairData(Repair singleRepairData) {
        RepairAdapter repairAdapter = new RepairAdapter(getApplicationContext());
        getLayoutInflater().inflate(R.layout.activity_repair, (FrameLayout) findViewById(R.id.content_frame));
        repairAdapter.initView(findViewById(R.id.activity_repair));
        repairAdapter.setData(singleRepairData);
        setTitle("Repair " + singleRepairData.getRepairHash());
    }

    @Override
    public void serviceFail(Exception exception) {
        exception.printStackTrace();
        progressDialog.hide();
        Toast.makeText(this , exception.getMessage(), Toast.LENGTH_LONG).show();
    }
}
