package seczkowski.mateusz.carservice.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Iterator;
import java.util.List;

import seczkowski.mateusz.carservice.R;
import seczkowski.mateusz.carservice.adapters.SpinnerClientXCarsAdapter;
import seczkowski.mateusz.carservice.adapters.SpinnerMechanicsAdapter;
import seczkowski.mateusz.carservice.exceptions.ServiceFailedHelper;
import seczkowski.mateusz.carservice.helpers.CustomAlertDialog;
import seczkowski.mateusz.carservice.helpers.ToStringHelper;
import seczkowski.mateusz.carservice.interfaces.IPostRepairRequestAPICallback;
import seczkowski.mateusz.carservice.interfaces.IGetClientXCarsAPICallback;
import seczkowski.mateusz.carservice.interfaces.IGetMechanicsAPICallback;
import seczkowski.mateusz.carservice.model.ClientXCar;
import seczkowski.mateusz.carservice.model.Mechanic;
import seczkowski.mateusz.carservice.model.RepairRequest;
import seczkowski.mateusz.carservice.model.User;
import seczkowski.mateusz.carservice.services.GetClientXCarsAPIService;
import seczkowski.mateusz.carservice.services.GetMechanicAPIService;
import seczkowski.mateusz.carservice.services.PostRepairRequestAPIService;

public class NewRepairRequestActivity extends MenuActivity implements IPostRepairRequestAPICallback, IGetClientXCarsAPICallback, IGetMechanicsAPICallback {
    private PostRepairRequestAPIService postRepairRequestAPIService;
    private GetMechanicAPIService getMechanicAPIService;
    private GetClientXCarsAPIService getClientXCarsAPIService;
    private ProgressDialog progressDialog;
    SharedPreferences settings;
    private Button addRepairRequestButton;
    private EditText repairRequestComment;
    private Spinner repairRequestSelectCarSpinner, repairRequestSelectMechanicSpinner;
    private SpinnerClientXCarsAdapter spinnerClientXCarsAdapter;
    private SpinnerMechanicsAdapter spinnerMechanicsAdapter;
    private User user;
    private Activity activity;
    ClientXCar[] carsArray;
    Mechanic[] mechanicsArray;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_repair_request);
        getWindow().setBackgroundDrawableResource(R.drawable.background_new_repair_req); // here instead of in layout to allow not shrinking background and still see keyboard on view starting
                                                                                        // Reported to Google as an ScrollView bug
        settings = getSharedPreferences(getString(R.string.KEY_CARSERVICE_APP_PREF), 0);
        user = (User) getIntent().getSerializableExtra(getString(R.string.KEY_USER));
        activity = this;

        getClientXCars();
    }

    private void getClientXCars() {
        getClientXCarsAPIService = new GetClientXCarsAPIService(this, this);
        getClientXCarsAPIService.downloadClientXCarsCarsByUserId(user.getId());
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.progress_loading_cars));
        progressDialog.show();
    }

    @Override
    public void getClientsCarsListServiceSuccess(ClientXCar clientXCar) {
        progressDialog.hide();
        List<ClientXCar> clientXCarsListFromDownloadedObject = clientXCar.getClientXCars();

        for (Iterator<ClientXCar> iter = clientXCarsListFromDownloadedObject.listIterator(); iter.hasNext(); ) {
            ClientXCar cxc = iter.next();
            if (!cxc.isActive()) {
                iter.remove();
            }
        }

        ClientXCar[] allocateSize = new ClientXCar[clientXCarsListFromDownloadedObject.size()];
        carsArray = clientXCarsListFromDownloadedObject.toArray(allocateSize);

        getMechanics();
    }

    @Override
    public void getClientsCarsListServiceFail(Exception exception) {
        progressDialog.hide();
        new ServiceFailedHelper(exception, this).showToast();
    }

    private void getMechanics() {
        getMechanicAPIService = new GetMechanicAPIService(this, this);
        getMechanicAPIService.downloadAllMechanics();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.progress_loading_mechanics));
        progressDialog.show();
    }

    @Override
    public void getMechanicsServiceSuccess(Mechanic mechanic) {
        progressDialog.hide();
        List<Mechanic> mechanicsListFromDownloadedObject = mechanic.getMechanicsList();
        Mechanic[] allocateSize = new Mechanic[mechanicsListFromDownloadedObject.size()];

        mechanicsArray = mechanicsListFromDownloadedObject.toArray(allocateSize);

        setView();
    }

    @Override
    public void getMechanicsServiceFail(Exception exception) {
        progressDialog.hide();
        new ServiceFailedHelper(exception, this).showToast();
    }

    private void setView() {
        addRepairRequestButton = (Button) findViewById(R.id.add_repair_request_add_button);
        addRepairRequestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateFields()) {
                    if(commentFieldEmpty())
                        sendNewRepairRequest();
                    else {
                        new CustomAlertDialog().showMessageYesCancel(activity,
                                getString(R.string.constraints_repair_request_withoutcomment),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        sendNewRepairRequest();
                                    }
                                });
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(), R.string.constraints_repair_request, Toast.LENGTH_LONG).show();
                }
            }
        });

        spinnerClientXCarsAdapter = new SpinnerClientXCarsAdapter(this, R.layout.spinner_with_label, carsArray); //default layout of spinners is android.R.layout.simple_spinner_item
        repairRequestSelectCarSpinner = (Spinner) findViewById(R.id.repair_request_select_clientxcar);
        repairRequestSelectCarSpinner.setAdapter(spinnerClientXCarsAdapter);
        Integer defaultCarPosition = spinnerClientXCarsAdapter.getDefaultCarPosition();
        if (defaultCarPosition != -1) {
            repairRequestSelectCarSpinner.setSelection(defaultCarPosition, true); // set default car if exists
        }

        spinnerMechanicsAdapter = new SpinnerMechanicsAdapter(this, R.layout.spinner_with_label, mechanicsArray);
        repairRequestSelectMechanicSpinner = (Spinner) findViewById(R.id.repair_request_select_mechanic);
        repairRequestSelectMechanicSpinner.setAdapter(spinnerMechanicsAdapter);

        repairRequestComment = (EditText) findViewById(R.id.repair_request_add_comment);
    }

    private boolean validateFields() {
        return !(repairRequestSelectCarSpinner.getSelectedItem().toString().isEmpty() || repairRequestSelectMechanicSpinner.getSelectedItem().toString().isEmpty());
    }

    private boolean commentFieldEmpty() {
        return !repairRequestComment.getText().toString().isEmpty();
    }

    private void sendNewRepairRequest() {
        postRepairRequestAPIService = new PostRepairRequestAPIService(this, this);
        try {
            RepairRequest repairRequest = new RepairRequest(
                    ((Mechanic) repairRequestSelectMechanicSpinner.getSelectedItem()).getMechanicId(),
                    ((ClientXCar) repairRequestSelectCarSpinner.getSelectedItem()).getClientXCarId(),
                    repairRequestComment.getText().toString()
            );

            postRepairRequestAPIService.sendRepairRequest(repairRequest, user.getId());
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getString(R.string.progress_sending_request));
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addRepairRequestServiceSuccess(RepairRequest repairRequest) {
        progressDialog.hide();
        new AlertDialog.Builder(this)
                .setMessage(getString(R.string.success_request_repair)
                        + "\nId: " + repairRequest.getId()
                        + "\n" + repairRequest.getIncomingRepairDateTime()
                        + "\n" + ToStringHelper.ObjectDataToString(activity, repairRequest.getMessageFromClient())
                ).setPositiveButton("OK", null).show();
    }

    @Override
    public void addRepairRequestServiceFail(Exception exception) {
        progressDialog.hide();
        new ServiceFailedHelper(exception, this).showDialogOk();
    }
}
