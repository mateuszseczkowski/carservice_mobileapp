package seczkowski.mateusz.carservice.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import seczkowski.mateusz.carservice.R;

public class MenuActivity extends AppCompatActivity{

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        setActionBar();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                SharedPreferences settings = getSharedPreferences(getString(R.string.KEY_CARSERVICE_APP_PREF), 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString(getString(R.string.KEY_REMEMBERED_LOGIN), "");
                editor.putString(getString(R.string.KEY_OAUTH_TOKEN), "");
                editor.apply();

                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;

            case R.id.refresh:
                finish();
                startActivity(getIntent());
                break;

            case R.id.action_add:
                Intent i = new Intent(getApplicationContext(), NewRepairRequestActivity.class);
                i.putExtra(getString(R.string.KEY_USER), getIntent().getSerializableExtra(getString(R.string.KEY_USER)));
                startActivity(i);
                break;

            case android.R.id.home:
                this.finish(); // close this activity and return to preview activity (if there is any)
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setActionBar() {
        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true); //BSc: displays back button
            bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(getString(R.string.action_bar_after_login_color))));
        }
    }
}
