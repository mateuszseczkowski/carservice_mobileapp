package seczkowski.mateusz.carservice.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import seczkowski.mateusz.carservice.R;
import seczkowski.mateusz.carservice.activities.RepairActivity;
import seczkowski.mateusz.carservice.adapters.InvoicesListItemAdapter;
import seczkowski.mateusz.carservice.interfaces.IGetInvoicesListAPICallback;
import seczkowski.mateusz.carservice.model.Invoice;
import seczkowski.mateusz.carservice.model.InvoicesList;
import seczkowski.mateusz.carservice.model.User;
import seczkowski.mateusz.carservice.services.GetInvoicesListAPIService;

public class InvoicesFragment extends Fragment implements IGetInvoicesListAPICallback {
    private SwipeRefreshLayout swipeContainer;
    private Context context;
    private Integer invoicesCount;
    private Invoice[] invoicesArray;
    private GetInvoicesListAPIService service;
    private ListView list;
    private User user;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invoices_list, container, false);

        list = (ListView) view.findViewById(R.id.list_of_invoices);
        context = getActivity().getApplicationContext();
        user = (User) getActivity().getIntent().getSerializableExtra(getString(R.string.KEY_USER));

        setSwipeContainer(view);

        return view;
    }

    private void setSwipeContainer(final View v) {
        swipeContainer = (SwipeRefreshLayout) v.findViewById(R.id.swipeContainer);

        getInvoicesListData();

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getInvoicesListData();
            }
        });

        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_dark,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    private void getInvoicesListData() {
        service = new GetInvoicesListAPIService(this, context);
        service.downloadInvoicesListData((user.getId()));
    }

    @Override
    public void getInvoicesServiceSuccess(InvoicesList invoicesList) {
        List<Invoice> invoices = invoicesList.getInvoices();
        Invoice[] allocateSize = new Invoice[invoices.size()];

        this.invoicesArray = invoices.toArray(allocateSize);
        invoicesCount = invoicesList.getCount();
        listInvoices(list);
        swipeContainer.setRefreshing(false);
    }

    private void listInvoices(final ListView list) {
        try {
            InvoicesListItemAdapter invoicesListItemAdapter = new InvoicesListItemAdapter(context, R.layout.item_invoice, invoicesArray, getActivity());
            list.setAdapter(invoicesListItemAdapter);

            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {
                    Intent i = new Intent(context, RepairActivity.class); //todo: create invoice activity and open extended invoice info
                    i.putExtra(getString(R.string.KEY_REPAIR_ID), invoicesArray[position].getRepairID());
                    i.putExtra(getString(R.string.KEY_USER), user);
                    startActivity(i);
                }
            });
        } catch (NullPointerException e) {
            serviceFail(e);
            e.printStackTrace();
        }
    }

    @Override
    public void serviceFail(Exception exception) {
        exception.printStackTrace();
        swipeContainer.setRefreshing(false);
        Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_LONG).show();
    }
}
