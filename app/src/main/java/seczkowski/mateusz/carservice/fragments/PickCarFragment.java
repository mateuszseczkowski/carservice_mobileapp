package seczkowski.mateusz.carservice.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import seczkowski.mateusz.carservice.R;
import seczkowski.mateusz.carservice.adapters.CarsListItemAdapter;
import seczkowski.mateusz.carservice.exceptions.ServiceFailedHelper;
import seczkowski.mateusz.carservice.helpers.CustomAlertDialog;
import seczkowski.mateusz.carservice.interfaces.IGetClientXCarsAPICallback;
import seczkowski.mateusz.carservice.interfaces.IPutClientXCarFlagsAPICallback;
import seczkowski.mateusz.carservice.model.ClientXCar;
import seczkowski.mateusz.carservice.model.User;
import seczkowski.mateusz.carservice.services.GetClientXCarsAPIService;
import seczkowski.mateusz.carservice.services.PutClientXCarFlagsAPIService;

public class PickCarFragment extends Fragment implements IGetClientXCarsAPICallback, IPutClientXCarFlagsAPICallback {

    private View view;
    private View alertLayout;
    private Context context;
    private User user;
    private SharedPreferences settings;
    SharedPreferences.Editor editor;
    private GetClientXCarsAPIService getClientXCarsAPIService;
    private PutClientXCarFlagsAPIService putClientXCarFlagsAPIService;
    private ProgressDialog progressDialog;
    private ListView carsListView;
    private Button refreshListOfCars, changeClientsCarStatusButton;
    private CheckBox dontShowAgainCheckbox;
    private AlertDialog.Builder alertDialogWithPositiveButton;
    private TextView defaultCarTextView;
    private TextInputEditText selectedClientsCarEditText;
    private Integer clientXCarsCount;
    private boolean showMessageAboutCarActivation;
    private ClientXCar[] clientXCarsArray;
    private ClientXCar defaultCar;
    private ClientXCar selectedCXC;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_settings_pick_car, container, false);

        context = getActivity().getApplicationContext();
        user = (User)getActivity().getIntent().getSerializableExtra(getString(R.string.KEY_USER));
        settings = context.getSharedPreferences(getString(R.string.KEY_CARSERVICE_APP_PREF), 0);
        editor = settings.edit();

        initializeView(user.getId());
        getClientsCarsData(user.getId());

        return view;
    }

    private void initializeView(final int userId) {
        refreshListOfCars = (Button) view.findViewById(R.id.settings_refresh_list_of_cars);
        refreshListOfCars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getClientsCarsData(userId);
            }
        });
        selectedClientsCarEditText = (TextInputEditText) view.findViewById(R.id.setting_car_selected_name);
        selectedClientsCarEditText.setKeyListener(null); //makes the input element not editable
        refreshListOfCars = (Button) view.findViewById(R.id.settings_refresh_list_of_cars);
        refreshListOfCars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getClientsCarsData(userId);
            }
        });
        changeClientsCarStatusButton = (Button) view.findViewById(R.id.settings_clientscar_change_status);
        changeClientsCarStatusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedCXC != null) {
                    if (selectedCXC.isActive()) {
                        selectedCXC.setActive(false);
                        if (selectedCXC.isDefault()) {
                            selectedCXC.setDefault(false);
                            defaultCarTextView.setText("");
                        }
                    } else {
                        selectedCXC.setActive(true);
                    }
                    updateClientsCarFlags(selectedCXC);
                }
            }
        });
        carsListView = (ListView) view.findViewById(R.id.setting_list_of_cars);
        defaultCarTextView = (TextView) view.findViewById(R.id.setting_default_car_label);

        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        alertLayout = layoutInflater.inflate(R.layout.checkbox, null);
        dontShowAgainCheckbox = (CheckBox) alertLayout.findViewById(R.id.checkboxitem);
        dontShowAgainCheckbox.setText(R.string.constraints_do_not_show_again_label);
        alertDialogWithPositiveButton = new AlertDialog.Builder(getActivity())
                .setView(alertLayout)
                .setTitle(R.string.attention)
                .setMessage(R.string.constraints_set_as_default_car)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (dontShowAgainCheckbox.isChecked()){
                            showMessageAboutCarActivation = false;
                        }
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putBoolean(getString(R.string.KEY_SHOWMESSAGE_ABOUT_CARACTIVATION), showMessageAboutCarActivation);
                        editor.apply();
                    }
                })
                .setNegativeButton(R.string.cancel, null);
    }

    private void getClientsCarsData(int userId) {
        getClientXCarsAPIService = new GetClientXCarsAPIService(this, context);
        getClientXCarsAPIService.downloadClientXCarsCarsByUserId(userId);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.progress_loading_data));
        progressDialog.show();
    }

    @Override
    public void getClientsCarsListServiceSuccess(ClientXCar clientXCar) {
        progressDialog.hide();
        List<ClientXCar> clientXCars = clientXCar.getClientXCars();
        ClientXCar[] allocateSize = new ClientXCar[clientXCars.size()];

        clientXCarsArray = clientXCars.toArray(allocateSize);
        clientXCarsCount = clientXCar.getCount();

        for (ClientXCar cxc : clientXCars) {
            if (cxc.isDefault()) {
                defaultCar = cxc;
                break;
            }
        }
        if (defaultCar != null)
            defaultCarTextView.setText(defaultCar.getClientXCarBasicData());

        listCars(carsListView);
    }

    private void updateClientsCarFlags (ClientXCar selectedCXC) {
        putClientXCarFlagsAPIService = new PutClientXCarFlagsAPIService(this, context);
        putClientXCarFlagsAPIService.updateClientXCarFlags(selectedCXC);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.progress_sending_request));
        progressDialog.show();
    }

    @Override
    public void updateClientXCarFlagsServiceSuccess(ClientXCar clientXCar) {
        progressDialog.hide();
        listCars(carsListView);
        if (clientXCar.isDefault())
            defaultCarTextView.setText(clientXCar.getClientXCarBasicData());
        Toast.makeText(getActivity(), R.string.success_update_clientsxcar_flags, Toast.LENGTH_SHORT).show();
    }

    private void listCars(ListView carsListView) {
        try {
            final CarsListItemAdapter carsListItemAdapter = new CarsListItemAdapter(context, R.layout.item_car, clientXCarsArray, getActivity());
            carsListView.setAdapter(carsListItemAdapter);

            carsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> arg0, View viewInner, int position, long id) {
                    selectAndSetDefaultClientsCarEvent(position);
                }
            });

            carsListView.setOnTouchListener(new ListView.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    int action = event.getAction();
                    switch (action) {
                        case MotionEvent.ACTION_DOWN:
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            break;

                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                    v.onTouchEvent(event);
                    return true;
                }
            });
        } catch (NullPointerException e) {
            Toast.makeText(getActivity(), R.string.error_message_list_cars, Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private void selectAndSetDefaultClientsCarEvent(int position) {
        selectedCXC = clientXCarsArray[position];
        selectedClientsCarEditText.setText(selectedCXC.getClientXCarBasicData());

        if (selectedCXC.isActive()) {
            changeClientsCarStatusButton.setText(getString(R.string.deactivate));
            new CustomAlertDialog().showMessageYesNo(getActivity(),
                    getString(R.string.constraints_question_car_as_default),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            selectedCXC.setDefault(true);
                            updateClientsCarFlags(selectedCXC);
                        }
                    });
        }
        else {
            changeClientsCarStatusButton.setText(getString(R.string.activate));
            showMessageAboutCarActivation = settings.getBoolean(getString(R.string.KEY_SHOWMESSAGE_ABOUT_CARACTIVATION), true);
            if (showMessageAboutCarActivation) {
                try {
                    ((ViewGroup)alertLayout.getParent()).removeView(alertLayout);// https://stackoverflow.com/questions/6526874/call-removeview-on-the-childs-parent-first
                }
                catch(Exception e){e.printStackTrace();}
                alertDialogWithPositiveButton.show();
            }
        }
    }

    @Override
    public void getClientsCarsListServiceFail(Exception exception) {
        progressDialog.hide();
        new ServiceFailedHelper(exception, getActivity()).showToast();
        listCars(carsListView);
    }

    @Override
    public void updateClientXCarFlagsServiceFail(Exception exception) {
        progressDialog.hide();
        new ServiceFailedHelper(exception, getActivity()).showToast();
    }
}
