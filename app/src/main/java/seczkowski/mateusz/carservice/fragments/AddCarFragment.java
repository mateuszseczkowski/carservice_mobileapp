package seczkowski.mateusz.carservice.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import seczkowski.mateusz.carservice.R;
import seczkowski.mateusz.carservice.helpers.FormValidator;
import seczkowski.mateusz.carservice.interfaces.IPostClientXCarAPICallback;
import seczkowski.mateusz.carservice.model.Car;
import seczkowski.mateusz.carservice.model.ClientXCar;
import seczkowski.mateusz.carservice.model.User;
import seczkowski.mateusz.carservice.services.PostClientXCarAPIService;

public class AddCarFragment extends Fragment implements IPostClientXCarAPICallback {

    private PostClientXCarAPIService service;
    private ProgressDialog progressDialog;
    private Button addCarButton;
    private User user;
    private View view;
    Context context;
    EditText modelEditText, makeEditText, yearEditText, capacityEditText, bhpEditText, versionEditText, vinNoEditText, plateNoEditText;
    boolean isFormValid;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_settings_add_car, container, false);

        context = getActivity().getApplicationContext();
        user = (User)getActivity().getIntent().getSerializableExtra(getString(R.string.KEY_USER));

        setView();

        return view;
    }

    private void setView() {
        plateNoEditText = (EditText) view.findViewById(R.id.setting_add_car_plateNo);
        modelEditText = (EditText) view.findViewById(R.id.setting_add_car_model);
        makeEditText = (EditText) view.findViewById(R.id.setting_add_car_make);
        yearEditText = (EditText) view.findViewById(R.id.setting_add_car_year);
        capacityEditText = (EditText) view.findViewById(R.id.setting_add_car_capacity);
        bhpEditText = (EditText) view.findViewById(R.id.setting_add_car_bhp);
        versionEditText = (EditText) view.findViewById(R.id.setting_add_car_version);
        vinNoEditText = (EditText) view.findViewById(R.id.setting_add_car_vinno);

        final List<EditText> requiredFields = new ArrayList<EditText>(){{
            add(plateNoEditText);
            add(modelEditText);
            add(makeEditText);
            add(yearEditText);
            add(capacityEditText);
            add(bhpEditText);
            add(versionEditText);
            add(vinNoEditText);
        }};

        addCarButton = (Button) view.findViewById(R.id.settings_add_car_button);
        addCarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFormValid = FormValidator.validateRequiredFields(requiredFields, getActivity());
                if(isFormValid){
                    addCar(user.getId());
                }
            }
        });
    }

    private void addCar(int userId) {
        service = new PostClientXCarAPIService(this, context);
        try {
            Car car = new Car(
                    modelEditText.getText().toString(),
                    makeEditText.getText().toString(),
                    Integer.parseInt(yearEditText.getText().toString()),
                    Double.parseDouble(capacityEditText.getText().toString()),
                    Double.parseDouble(bhpEditText.getText().toString()),
                    versionEditText.getText().toString(),
                    vinNoEditText.getText().toString(),
                    true
            );

            ClientXCar clientXCar = new ClientXCar(
                    plateNoEditText.getText().toString(),
                    car
            );

        service.addClientXCar(clientXCar, userId);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.progress_sending_request));
        progressDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void serviceSuccess(int clientXCarID) {
        progressDialog.hide();
        new AlertDialog.Builder(getActivity())
                .setMessage(getResources().getString(R.string.success_added_car) + ". \nId: " + clientXCarID).setPositiveButton("OK", null).show();
    }

    @Override
    public void serviceFail(Exception exception) {
        exception.printStackTrace();
        progressDialog.hide();
        Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_LONG).show();
    }
}
