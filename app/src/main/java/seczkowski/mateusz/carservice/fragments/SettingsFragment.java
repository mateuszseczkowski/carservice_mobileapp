package seczkowski.mateusz.carservice.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.TabHost;
import android.widget.Toast;

import java.util.List;
import java.util.Vector;

import seczkowski.mateusz.carservice.R;
import seczkowski.mateusz.carservice.adapters.ViewPagerAdapter;
import seczkowski.mateusz.carservice.exceptions.ServiceFailedHelper;
import seczkowski.mateusz.carservice.interfaces.IGetUserAPICallback;
import seczkowski.mateusz.carservice.interfaces.IPutPersonalUserDataAPICallback;
import seczkowski.mateusz.carservice.model.User;
import seczkowski.mateusz.carservice.services.GetUserAPIService;
import seczkowski.mateusz.carservice.services.PutPersonalUserDataAPIService;

public class SettingsFragment extends Fragment implements TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener, IGetUserAPICallback, IPutPersonalUserDataAPICallback {

    private View view;
    private TabHost tabHost;
    private ViewPager viewPager;
    private SharedPreferences settings;
    private ProgressDialog progressDialog;
    Context context;
    private GetUserAPIService getUserService;
    private User user;
    private PutPersonalUserDataAPIService updateUserService;
    String login;
    TextInputEditText editSettingName, editSettingSurname;
    Button saveUserDataButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_settings, container, false);

        context = getActivity().getApplicationContext();
        settings = context.getSharedPreferences(getString(R.string.KEY_CARSERVICE_APP_PREF), 0);
        login = settings.getString(getString(R.string.KEY_CURRENT_USER_LOGIN), "");

        this.initSettingsUserDataControls();
        this.initTabHost();
        this.initViewPager();
        getUserDataByLogin(login);

        return view;
    }

    private void initSettingsUserDataControls(){
        editSettingName = (TextInputEditText) view.findViewById(R.id.setting_name);
        editSettingSurname = (TextInputEditText) view.findViewById(R.id.setting_surname);
        saveUserDataButton = (Button) view.findViewById(R.id.settings_save_user_personal_data);

        saveUserDataButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                updateUserData();
            }
        });
    }

    private void initTabHost() {

        tabHost = (TabHost) view.findViewById(android.R.id.tabhost);
        tabHost.setup();

        String[] namesOfFragments = {getString(R.string.pick_car), getString(R.string.add_car)};

        for (String fragmentName : namesOfFragments) {
            TabHost.TabSpec tabSpec;
            tabSpec = tabHost.newTabSpec(fragmentName);
            tabSpec.setIndicator(fragmentName);
            tabSpec.setContent(new FakeContent(getActivity()));
            tabHost.addTab(tabSpec);
        }

        tabHost.setOnTabChangedListener(this);
        tabHost.setBackgroundColor(Color.WHITE);
    }

    private void initViewPager() {
        List<Fragment> viewPagerFragments = new Vector<>();

        viewPagerFragments.add(new PickCarFragment());
        viewPagerFragments.add(new AddCarFragment());

        ViewPagerAdapter myViewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager(), viewPagerFragments);
        this.viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        this.viewPager.setAdapter(myViewPagerAdapter);
        this.viewPager.setBackgroundColor(Color.BLACK);
        this.viewPager.addOnPageChangeListener(this);

        int limit = (myViewPagerAdapter.getCount() > 1 ? myViewPagerAdapter.getCount() - 1 : 1);
        this.viewPager.setOffscreenPageLimit(limit); //allows to prevent the refreshing of the view pager fragments
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        this.tabHost.setCurrentTab(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onTabChanged(String tabId) {
        int pos = this.tabHost.getCurrentTab();
        this.viewPager.setCurrentItem(pos);

        HorizontalScrollView hScrollView = (HorizontalScrollView) view.findViewById(R.id.hScrollView);
        View tabView = tabHost.getCurrentTabView();
        int scrollPos = (tabView != null ? tabView.getLeft() : 0) - (hScrollView.getWidth() - (tabView != null ? tabView.getWidth() : 0)) / 2;
        hScrollView.smoothScrollTo(scrollPos, 0);
    }

    private class FakeContent implements TabHost.TabContentFactory {
        private final Context mContext;

        FakeContent(Context context) {
            mContext = context;
        }

        @Override
        public View createTabContent(String tag) {
            View v = new View(mContext);
            v.setMinimumHeight(0);
            v.setMinimumWidth(0);
            return v;
        }
    }

    private void getUserDataByLogin(String login) {
        getUserService = new GetUserAPIService(this, context);
        getUserService.getUserDataByLogin(login);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.progress_loading_data));
        progressDialog.show();
    }

    @Override
    public void getUserDataServiceSuccess(User user) {
        progressDialog.hide();
        initMainSettingsFields(user.getName(), user.getSurname());
        this.user = user;
    }

    private void initMainSettingsFields(String name, String surname) {
        editSettingName.setText(name);
        editSettingSurname.setText(surname);
    }

    private void updateUserData() {
        getDataFromFormAndUpdateUserObject();
        updateUserService = new PutPersonalUserDataAPIService(this, context);
        updateUserService.updateUserPersonalData(this.user);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.progress_sending_request));
        progressDialog.show();
    }

    private void getDataFromFormAndUpdateUserObject(){
        this.user.setName(editSettingName.getText().toString());
        this.user.setSurname(editSettingSurname.getText().toString());
    }

    @Override
    public void updateUserDataServiceSuccess(User user) {
        progressDialog.hide();
        Toast.makeText(getActivity(), R.string.success_update_user_data, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getUserDataServiceFail(Exception exception) {
        progressDialog.hide();
        new ServiceFailedHelper(exception, getString(R.string.error_message_get_user), getActivity())
                .showDialogOk();
    }

    @Override
    public void updateUserDataServiceFail(Exception exception) {
        progressDialog.hide();
        new ServiceFailedHelper(exception, getString(R.string.error_message_update_user), getActivity())
                .showDialogOk();
    }
}