package seczkowski.mateusz.carservice.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.PopupMenu;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.util.List;

import seczkowski.mateusz.carservice.R;
import seczkowski.mateusz.carservice.activities.RepairActivity;
import seczkowski.mateusz.carservice.adapters.RepairsListViewAdapter;
import seczkowski.mateusz.carservice.exceptions.ServiceFailedHelper;
import seczkowski.mateusz.carservice.helpers.Data.DBHelper;
import seczkowski.mateusz.carservice.helpers.Data.SQLiteDatabaseWrapper;
import seczkowski.mateusz.carservice.helpers.NetworkConnectionHelper;
import seczkowski.mateusz.carservice.interfaces.IGetRepairsAPICallback;
import seczkowski.mateusz.carservice.model.Repair;
import seczkowski.mateusz.carservice.model.Repairs;
import seczkowski.mateusz.carservice.model.User;
import seczkowski.mateusz.carservice.services.GetRepairsAPIService;

import static seczkowski.mateusz.carservice.helpers.Enums.OrderType.*;

public class RepairsFragment extends Fragment implements IGetRepairsAPICallback, PopupMenu.OnMenuItemClickListener {

    private Context _context;
    private DBHelper _db = null;
    SQLiteDatabaseWrapper _dbWrapper;
    boolean isOnline;

    private SwipeRefreshLayout swipeContainer;
    private Integer repairsCount;
    private List<Repair> repairsList;
    private TextInputEditText filterRepairsListEditText;
    private ListView repairsListView;
    RepairsListViewAdapter repairsListAdapter;
    private User user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_sort).setVisible(true);
        menu.findItem(R.id.action_add).setVisible(true);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort:
                showPopupMenu(getActivity().findViewById(R.id.action_sort));
                return true;
            default:
                break;
        }
        return false;
    }

    public void showPopupMenu(View anchor) {
        PopupMenu popup = new PopupMenu(getActivity(), anchor);

        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.popup_sort_menu);
        popup.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.start_asc:
                repairsListAdapter.sort(START_DATE_ASC);
                return true;
            case R.id.start_desc:
                repairsListAdapter.sort(START_DATE_DESC);
                return true;
            case R.id.id_asc:
                repairsListAdapter.sort(ID_ASC);
                return true;
            case R.id.id_desc:
                repairsListAdapter.sort(ID_DESC);
                return true;
            default:
                return false;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_repairs_list, container, false);
        _context = getActivity();
        _db = getHelper();
        _dbWrapper = new SQLiteDatabaseWrapper(_db);

        initializeView(view);

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (_db != null) {
            OpenHelperManager.releaseHelper();
            _db = null;
        }
    }

    private DBHelper getHelper() {
        if (_db == null) {
            _db = OpenHelperManager.getHelper(getActivity(), DBHelper.class);
        }
        return _db;
    }

    private void initializeView(View view) {
        _context = getActivity().getApplicationContext();
        repairsListView = (ListView) view.findViewById(R.id.list_of_repairs);
        user = (User) getActivity().getIntent().getSerializableExtra(getString(R.string.KEY_USER));
        filterRepairsListEditText = (TextInputEditText)  view.findViewById(R.id.repairs_filter_edit_text);

        getRepairsFromApi();
        setSwipeContainer(view);
    }

    private void getRepairsFromApi() {
        GetRepairsAPIService service = new GetRepairsAPIService(this, _context);
        service.downloadRepairsListOfUser(user.getId());
    }

    private void setSwipeContainer(final View v) {
        swipeContainer = (SwipeRefreshLayout) v.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getRepairsFromApi();
            }
        });

        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_dark,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    @Override
    public void serviceSuccess(Repairs repairs) {
        swipeContainer.setRefreshing(false);

        repairsList = repairs.getRepairsList();
        repairsCount = repairs.getCount();
        displayRepairsOnListView(repairsListView);

        _dbWrapper.putRepairsListLiteAsync(repairsList);
    }

    private void displayRepairsOnListView(final ListView list) {
        try {
            repairsListAdapter = new RepairsListViewAdapter(_context, R.layout.item_repair, repairsList, getActivity());
            list.setAdapter(repairsListAdapter);

            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {
                    Intent i = new Intent(_context, RepairActivity.class);
                    i.putExtra(getString(R.string.KEY_REPAIR_ID), repairsListAdapter.getItem(position).getRepairID());
                    i.putExtra(getString(R.string.KEY_USER), user);
                    startActivity(i);
                }
            });

            filterRepairsListEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    repairsListAdapter.getFilter().filter(s.toString());
                }
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {}
                @Override
                public void afterTextChanged(Editable s) {}
            });
        } catch (NullPointerException e) {
            Toast.makeText(getActivity(), R.string.error_message_list_repairs, Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    @Override
    public void serviceFail(Exception exception) {
        swipeContainer.setRefreshing(false);

        exception.printStackTrace();

        repairsList = _dbWrapper.getRepairsListLite();
        displayRepairsOnListView(repairsListView);

        isOnline = new NetworkConnectionHelper(_context).isOnline();
        if (!isOnline) {
            new ServiceFailedHelper(exception,
                    getString(R.string.constraints_connection_with_server_violetion_offline_data), getActivity())
                    .showToast();
            return;
        }
        new ServiceFailedHelper(exception, getActivity()).showToast();
    }
}