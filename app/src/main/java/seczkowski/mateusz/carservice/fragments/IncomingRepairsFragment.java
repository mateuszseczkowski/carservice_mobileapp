package seczkowski.mateusz.carservice.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import seczkowski.mateusz.carservice.R;
import seczkowski.mateusz.carservice.adapters.IncomingRepairsListItemAdapter;
import seczkowski.mateusz.carservice.interfaces.IGetIncomingRepairsListAPICallback;
import seczkowski.mateusz.carservice.model.IncomingRepair;
import seczkowski.mateusz.carservice.model.IncomingRepairsList;
import seczkowski.mateusz.carservice.model.User;
import seczkowski.mateusz.carservice.services.GetIncomingRepairsListAPIService;

public class IncomingRepairsFragment extends Fragment implements IGetIncomingRepairsListAPICallback {
    private SwipeRefreshLayout swipeContainer;
    private Context context;
    private IncomingRepair[] incomingRepairsArray;
    private GetIncomingRepairsListAPIService service;
    private ListView list;
    private User user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_add).setVisible(true);
        super.onPrepareOptionsMenu(menu);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_incoming_repairs_list, container, false);

        list = (ListView) view.findViewById(R.id.list_of_incoming_repairs);
        context = getActivity().getApplicationContext();
        user = (User) getActivity().getIntent().getSerializableExtra(getString(R.string.KEY_USER));

        setSwipeContainer(view);

        return view;
    }

    private void setSwipeContainer(final View v) {
        swipeContainer = (SwipeRefreshLayout) v.findViewById(R.id.swipeContainer);

        getIncomingRepairsListData();

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getIncomingRepairsListData();
            }
        });

        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_dark,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    private void getIncomingRepairsListData() {
        service = new GetIncomingRepairsListAPIService(this, context);
        service.downloadIncomingRepairsListOfMechanic((user.getMechanicId()));
    }

    @Override
    public void getIncomingRepairsServiceSuccess(IncomingRepairsList incomingRepairsList) {
        List<IncomingRepair> incomingRepairs = incomingRepairsList.getIncomingRepairs();
        IncomingRepair[] allocateSize = new IncomingRepair[incomingRepairs.size()];

        this.incomingRepairsArray = incomingRepairs.toArray(allocateSize);
        listIncomingRepairs(list);
        swipeContainer.setRefreshing(false);
    }

    private void listIncomingRepairs(final ListView list) {
        try {
            IncomingRepairsListItemAdapter incomingRepairsListItemAdapter = new IncomingRepairsListItemAdapter
                    (context, R.layout.item_incoming_repair, incomingRepairsArray, getActivity());
            list.setAdapter(incomingRepairsListItemAdapter);

            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {
                    //do nothing
                }
            });
        } catch (NullPointerException e) {
            serviceFail(e);
            e.printStackTrace();
        }
    }

    @Override
    public void serviceFail(Exception exception) {
        exception.printStackTrace();
        swipeContainer.setRefreshing(false);
        Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_LONG).show();
    }
}
