package seczkowski.mateusz.carservice.helpers;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class NetworkConnectionHelperTest {
    @Test
    public void isOnline() {
        Context appContext = InstrumentationRegistry.getTargetContext();

        boolean isOnline = new NetworkConnectionHelper(appContext).isOnline();

        assertEquals(isOnline, true);
    }
}