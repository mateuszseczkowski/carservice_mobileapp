package seczkowski.mateusz.carservice.helpers;

import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import seczkowski.mateusz.carservice.R;
import seczkowski.mateusz.carservice.activities.MainActivity;
import seczkowski.mateusz.carservice.model.Role;
import seczkowski.mateusz.carservice.model.User;

@RunWith(AndroidJUnit4.class)
public class CustomAlertDialogTest { // The test firstly creates the MainActivity only to test the Toast creation (as a "container" - activity passed to the parameters of custom alert dialog)
    private CustomAlertDialog customAlertDialog;
    private MainActivity activity;

    @Rule
    public ActivityTestRule<MainActivity> mainActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class){
        @Override
        protected Intent getActivityIntent() { // This is to deal with the extras when the Activity is created as a test rule
            Context context = InstrumentationRegistry.getTargetContext();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.putExtra(context.getString(R.string.KEY_USER), new User(new Role(2)));
            return intent;
        }
    };

    @Before
    public void setUp() {
        customAlertDialog = new CustomAlertDialog();
        activity = mainActivityTestRule.getActivity();
    }

    @Test
    public void showMessageOk() throws Exception {
        Assert.assertNotNull(customAlertDialog);
        Looper.prepare(); // This is needed when the Toast is created in a non-UI thread like in this test. "Can't create handler inside thread that has not called Looper.prepare()"
        customAlertDialog.showMessageOk(activity, "FAKE_MESSAGE");
    }
}