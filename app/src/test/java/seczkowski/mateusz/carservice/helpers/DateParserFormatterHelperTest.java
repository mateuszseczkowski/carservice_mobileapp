package seczkowski.mateusz.carservice.helpers;

import junit.framework.Assert;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.mock;

import org.junit.runner.RunWith;
import org.mockito.Mock;

import java.util.Date;

public class DateParserFormatterHelperTest {
    public static String TEST_PARSED_DATE = "";

    @Test
    public void parseDateFromString() throws Exception {
        Date parsedDate = DateParserFormatterHelper.ParseDateFromString("2017-02-01T00:00:00");
        Assert.assertNotNull(parsedDate);
    }
}